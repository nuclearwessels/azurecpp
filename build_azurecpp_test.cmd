@echo off

msbuild.exe /p:Platform=x64 /p:Configuration=Release "azure_cpp.sln"
IF %ERRORLEVEL% NEQ 0 EXIT %ERRORLEVEL% 

mkdir dist
mkdir dist\test
mkdir dist\test\azure_cpp_test
copy x64\Release\azure_cpp_test.exe dist\test\azure_cpp_test
IF %ERRORLEVEL% NEQ 0 EXIT %ERRORLEVEL% 

copy x64\Release\azure_cpp.dll dist\test\azure_cpp_test
IF %ERRORLEVEL% NEQ 0 EXIT %ERRORLEVEL% 

copy x64\Release\libcurl-x64.dll dist\test\azure_cpp_test
IF %ERRORLEVEL% NEQ 0 EXIT %ERRORLEVEL% 

copy x64\Release\curl-ca-bundle.crt dist\test\azure_cpp_test
IF %ERRORLEVEL% NEQ 0 EXIT %ERRORLEVEL% 

