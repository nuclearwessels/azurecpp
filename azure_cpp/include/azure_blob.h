#pragma once


#include <map>
#include <string>
#include <vector>
#include <memory>
#include <regex>
#include <vector>
#include <ctime>
#include <optional>

#include <tinyxml2.h>

#include "curl_wrapper.h"
#include "azure_cpp_core.h"
#include "azure_blob_list.h"
#include "azure_blob_blobops.h"


namespace azurecpp
{


class AZURECPPLIB SASToken
{
public:
    SASToken(const std::string& sasToken);

    const std::string& sasToken() const;

    const std::map<std::string, std::string>& parts() const;

    std::string value(const std::string& part) const;

private:
    std::string _sasToken;
    std::map<std::string, std::string> _parts;
};



class AZURECPPLIB GetBlobResponse : public CurlResponseData
{
public:
    GetBlobResponse();
    GetBlobResponse(const CurlResponseData& crd);
    GetBlobResponse(CurlResponseData&& crd);
    //GetBlobResponse& operator = (CurlResponseData& crd);
    //GetBlobResponse& operator = (CurlResponseData&& crd);

    void setHeaders(const std::string& headers) override;

    time_t lastModified() const;

private:
    void updateFields();

    static void updateField(const std::map<std::string, std::string>& map, const std::string& header, std::string& field);
    
    time_t _lastModified;
    time_t _creationTime;
    
    std::map<std::string, std::string> _metadata;

    int64_t _contentLength;
    std::string _contentType;
    
    std::string _contentRange;
    std::string _etag;
    std::string _contentMD5;

    std::string _contentCRC64;
    std::string _contentEncoding;
    std::string _contentLanguage;
    std::string _cacheControl;
    std::string _contentDisposition;

    std::optional<int64_t> _sequenceNumber;

    BlobType _blobType;

    std::optional<time_t> _copyCompletionTime;
    std::string _copyStatusDescription;
    std::string _copyId;
    std::string _copyProgress;
    std::string _copySource;
    std::optional<CopyStatus> _copyStatus;
    std::optional<LeaseDuration> _leaseDuration;
    std::optional<LeaseState> _leaseState;
    std::optional<LeaseStatus> _leaseStatus;

    std::string _requestId;
    std::string _version;
    std::string _acceptRanges;

    time_t _date;
    std::string _accessControlAllowOrigin;
    std::string _accessControlExposeHeaders;
    std::string _accessControlAllowCredentials;
    std::string _vary;
    
    std::optional<int64_t> _committedBlockCount;

    bool _serverEncrypted;
    std::string _encryptionKeySHA256;
    std::string _encryptionScope;
    std::string _clientRequestId;
    std::string _blobContentMD5;

    std::optional<time_t> _lastAccessTime;
    std::string _blobSealed;
};


class AZURECPPLIB BlobService : protected CurlWrapper
{
public:
    BlobService();
    BlobService(const std::string& account);

    void setHttp(bool http);
    bool http() const;

    void setComputeMD5(bool computeMD5);
    bool computeMD5() const;

    void setComputeCRC64(bool computeCRC64);
    bool computeCRC64() const;

    void clear();

    void setSASToken(const std::string& sasToken);
    const std::string& sasToken() const;

    void setAccountKey(const std::string& accountKey);
    const std::string& accountKey() const;

    void setAccount(const std::string& account);
    const std::string& account() const;

    ListContainerResponse listContainers();
    
    ListBlobResponse listBlobs(const std::string& container, const ListBlobOptions& options);
    
    GetBlobResponse getBlob(const std::string& container, 
                            const std::string& blobName,
                            const GetBlobOptions& options);

    void getBlobFile(const std::string& container, const std::string& blobName,
                     const GetBlobOptions& options,
                     const std::string& filename);

    void putBlob(const std::string& container, const std::string& blobName, 
                 const PutBlobOptions& options,
                 const void* buffer, int buflen);

    void putBlock(const std::string& container, const std::string& blobName,
                  const std::string& blockId,
                  const PutBlockOptions& options,
                  const void* buffer, int buflen);

    void putBlockList(const std::string& container, const std::string& blobName,
                      const PutBlockListOptions& options,
                      const std::vector<PutBlockBlob>& blockIds);
    
    bool blobExists(const std::string& container, const std::string& blobName);

    void deleteBlob(const std::string& container, const std::string& blobName, const DeleteBlobOptions& options);

    void setBlobTags(const std::string& container, const std::string& blobName, const std::map<std::string, std::string>& tags);

    std::map<std::string, std::string> getBlobTags(const std::string& container, const std::string& blobName);

    static void setDefaultConnErrRetries(int retries);
    static int defaultConnErrRetries();

protected:
    std::string _account;
    std::string _sasToken;
    std::string _accountKey;
    bool _http;

    virtual CurlResponseData get() const override;
    //virtual CurlResponse getFile(const std::string& filename) const override;

    virtual CurlResponseData put(const std::string& data) const override;
    virtual CurlResponseData put(const void* buffer, size_t size) const override;
    //virtual CurlResponseData putFile(const std::string& filename) override;


    std::string stringToSignAccountKey(const std::string& verb, const std::string& resource) const;

    void addSecurityInfo(const std::string& verb, const std::string& resource);

    void setOptions(const PutBlobOptions& options, bool includeBlobType);

    static std::string encodeResource(const std::string& resource);

    static const std::string API_VERSION;
    static const std::string EOL;

private: 
    static int _defaultConnErrRetries;
    
};



}

