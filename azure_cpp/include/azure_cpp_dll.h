#pragma once

#ifdef AZURECPPDLL_EXPORTS
#define AZURECPPLIB __declspec(dllexport)
#else
#define AZURECPPLIB __declspec(dllimport)
#endif
