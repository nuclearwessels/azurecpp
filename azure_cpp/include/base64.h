

#pragma once

#include "azure_cpp_dll.h"

#include <string>

std::string AZURECPPLIB base64_encode(const std::string& s);
std::string AZURECPPLIB base64_decode(const std::string& s);
