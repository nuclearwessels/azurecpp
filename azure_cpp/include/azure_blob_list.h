
#pragma once

#include <map>
#include <optional>
#include <string>
#include <vector>

#include <tinyxml2.h>

#include "azure_cpp_core.h"
#include "azure_cpp_dll.h"


namespace azurecpp
{
    class AZURECPPLIB ContainerInfo
    {
    public:
        ContainerInfo();

        const std::string& name() const;

        void parseXML(tinyxml2::XMLElement* containerElement);

    private:
        std::string _name;

        std::optional<time_t> _lastModified;
        std::string _etag;
        std::string _leaseStatus;
        std::string _leaseState;
        std::string _defaultEncryptionScope;
        std::optional<bool> _denyEncryptionScopeOverride;
        std::optional<bool> _hasImmutabilityPolicy;
        std::optional<bool> _hasLegalHold;

        std::map<std::string, std::string> _metadata;
    };


    class AZURECPPLIB ListContainerResponse : public AzureResponseBase
    {
    public:
        ListContainerResponse();

        const std::vector<ContainerInfo>& containers() const;
        const std::string& prefix() const;

        void parseXML(tinyxml2::XMLDocument& doc);

    protected:
        std::vector<ContainerInfo> _containers;
        std::string _prefix;
    };


    class AZURECPPLIB BlobInfo
    {
    public:
        BlobInfo();

        void parseXML(tinyxml2::XMLElement* blobElement);

        const std::string& name() const;
        const time_t& creationTime() const;
        const time_t& lastModifiedTime() const;
        const std::string& etag() const;
        const long long& contentLength() const;
        const std::string& contentType() const;
        const std::string& contentEncoding() const;
        const std::string& contentLanguage() const;
        const std::string& contentMD5() const;
        const std::string& cacheControl() const;
        const std::string& sequenceNumber() const;
        const BlobType& blobType() const;
        const std::optional<AccessTier>& accessTier() const;
        const std::optional<LeaseState>& leaseState() const;
        const std::optional<LeaseStatus>& leaseStatus() const;
        const std::optional<LeaseDuration>& leaseDuration() const;
        const std::string& copyId() const;
        const std::optional<CopyStatus>& copyStatus() const;
        const std::string& copySource() const;
        const std::optional<long>& copyProgress() const;
        const std::optional<time_t>& copyCompletionTime() const;
        const std::string& copyStatusDesc() const;
        const std::optional<bool>& serverEncrypted() const;
        const std::string& customerProvidedKeySha256() const;
        const std::string& encryptionScope() const;
        const std::optional<bool>& incrementalCopy() const;
        const std::optional<bool>& accessTierInferred() const;
        const std::optional<time_t>& accessTierChangeTime() const;
        const std::optional<time_t>& deletedTime() const;
        const std::optional<int>& remainingRetentionDays() const;
        const std::optional<int>& tagCount() const;
        const std::string& rehydratePriority() const;

        const std::map<std::string, std::string>& metadata() const;
        const std::map<std::string, std::string>& tags() const;


    private:
        std::string _name;
        time_t _creationTime;
        time_t _lastModifiedTime;
        std::string _etag;
        long long _contentLength;
        std::string _contentType;
        std::string _contentEncoding;
        std::string _contentLanguage;
        std::string _contentMD5;
        std::string _cacheControl;
        std::string _sequenceNumber;
        BlobType _blobType;
        std::optional<AccessTier> _accessTier;
        std::optional<LeaseState> _leaseState;
        std::optional<LeaseStatus> _leaseStatus;
        std::optional<LeaseDuration> _leaseDuration;
        std::string _copyId;
        std::optional<CopyStatus> _copyStatus;
        std::string _copySource;
        std::optional<long> _copyProgress;
        std::optional<time_t> _copyCompletionTime;
        std::string _copyStatusDesc;
        std::optional<bool> _serverEncrypted;
        std::string _customerProvidedKeySha256;
        std::string _encryptionScope;
        std::optional<bool> _incrementalCopy;
        std::optional<bool> _accessTierInferred;
        std::optional<time_t> _accessTierChangeTime;
        std::optional<time_t> _deletedTime;
        std::optional<int> _remainingRetentionDays;
        std::optional<int> _tagCount;
        std::string _rehydratePriority;

        std::map<std::string, std::string> _metadata;
        std::map<std::string, std::string> _tags;

        static const std::map<std::string, BlobType> BLOB_TYPE_MAP;
        static const std::map<std::string, AccessTier> ACCESS_TIER_MAP;
        static const std::map<std::string, LeaseState> LEASE_STATE_MAP;
        static const std::map<std::string, LeaseStatus> LEASE_STATUS_MAP;
        static const std::map<std::string, LeaseDuration> LEASE_DURATION_MAP;
        static const std::map<std::string, CopyStatus> COPY_STATUS_MAP;

    };


    struct AZURECPPLIB ListBlobIncludeOptions
    {
        ListBlobIncludeOptions();

        bool snapshots, metadata, uncommittedblobs, copy, deleted, tags, versions;

        std::string str() const;
    };


    struct AZURECPPLIB ListBlobShowOnlyOptions
    {
        ListBlobShowOnlyOptions();

        bool deleted;

        std::string str() const;
    };

    struct AZURECPPLIB ListBlobOptions
    {
        ListBlobOptions();

        std::string prefix;
        std::string delimiter;
        std::string marker;
        std::optional<int> maxResults;

        ListBlobIncludeOptions includeOptions;
        ListBlobShowOnlyOptions showOnlyOptions;

        std::optional<int> timeout;
    };


    class AZURECPPLIB ListBlobResponse : public AzureResponseBase
    {
    public:
        ListBlobResponse();

        const std::vector<BlobInfo>& blobs() const;

        void parseXML(tinyxml2::XMLDocument& doc);

    protected:
        std::vector<BlobInfo> _blobs;
    };


}
