

#include "azure_cpp_core.h"
#include "curl_wrapper.h"

#include "azure_cpp_dll.h"


namespace azurecpp
{
    struct AZURECPPLIB CommonBlobOptionsBase
    {
        CommonBlobOptionsBase();

        std::string clientRequestId;
        std::string leaseId;

        std::optional<int> timeout;

        virtual void update(CurlWrapper& wrapper) const;
    };


    struct AZURECPPLIB CommonBlobOptions : public CommonBlobOptionsBase
    {
        CommonBlobOptions();

        std::string origin;

        void update(CurlWrapper& wrapper) const override;
    };


    struct AZURECPPLIB PutBlobOptionsBase : public CommonBlobOptions
    {
        std::string contentMD5;
        std::string contentCRC64;
        std::string encryptionScope;

        std::map<std::string, std::string> metadata;

        void update(CurlWrapper& wrapper) const override;
    };


    struct AZURECPPLIB PutBlockListOptions : public PutBlobOptionsBase
    {
        PutBlockListOptions();

        std::string contentType;
        std::string contentEncoding;
        std::string contentLanguage;
        std::string contentDisposition;

        std::string cacheControl;

        std::optional<AccessTier> accessTier;

        void update(CurlWrapper& wrapper) const override;
    };


    struct AZURECPPLIB PutBlobOptions : public PutBlockListOptions
    {
        PutBlobOptions();

        BlobType blobType;

        void update(CurlWrapper& wrapper) const override;
    };



    struct AZURECPPLIB PutBlockOptions : public PutBlobOptionsBase
    {
        PutBlockOptions();
    };





    enum class BlockListType
    {
        LATEST,
        COMMITTED,
        UNCOMMITTED
    };


    struct AZURECPPLIB PutBlockBlob
    {
        PutBlockBlob();

        std::string blockId;
        BlockListType blockType;

        tinyxml2::XMLElement* element(tinyxml2::XMLDocument& doc) const;

    };


    struct AZURECPPLIB Range
    {
        Range();

        long long start;
        std::optional<long long> end;

        std::string str() const;
    };



    struct AZURECPPLIB GetBlobOptions : public CommonBlobOptions
    {
        GetBlobOptions();

        std::optional<Range> range;        
        std::optional<bool> rangeContentMD5;
        std::optional<bool> rangeContentCRC64;

        std::optional<time_t> snapshot;
        std::optional<time_t> versionid;
        std::optional<int> timeout;

        void update(CurlWrapper& wrapper) const override;
    };


    enum class DeleteSnapshots
    {
        INCLUDE,
        ONLY
    };

    enum class DeleteType
    {
        PERMANENT
    };

    struct AZURECPPLIB DeleteBlobOptions : public CommonBlobOptionsBase
    {
        DeleteBlobOptions();

        DeleteSnapshots deleteSnapshots;

        std::string snapshot;

        std::optional<time_t> versionId;
        std::optional<DeleteType> deleteType;

        void update(CurlWrapper& wrapper) const override;

        static const std::map<DeleteSnapshots, std::string> DELETE_SNAPSHOTS_MAP_STRINGS;
        static const std::map<DeleteType, std::string> DELETE_TYPE_MAP_STRINGS;
    };


}