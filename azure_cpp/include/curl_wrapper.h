#pragma once
#include "azure_cpp_dll.h"

#include <curl/curl.h>

#include <memory>
#include <string>
#include <map>
#include <vector>


enum class HttpStatusCode
{
	Continue = 100,
	SwitchingProtocols = 101,
	Processing = 102,
	Checkpoint = 103,
	Ok = 200,
	Created = 201,
	Accepted = 202,
	NonAuthoritativeInformation = 203,
	NoContent = 204,
	ResetContent = 205,
	PartialContent = 206,
	MultiStatus = 207,
	AlreadyReported = 208,
	IMUsed = 226
};


enum class HttpResponseCategory
{
	INFORMATIONAL, SUCCESS, REDIRECTION, CLIENT_ERROR, SERVER_ERROR
};


class AZURECPPLIB CurlException : public std::exception
{
public:
	CurlException(const std::string& msg);

	const char* what() const override;

protected:
	CurlException();

	std::string _what;
};



class AZURECPPLIB CurlCodeException : public CurlException
{
public:
	CurlCodeException(CURLcode code);

	CURLcode curlCode() const;

	static const std::map<CURLcode, std::string> CURL_CODE_MAP;

private:
	CURLcode _curlCode;
};




class AZURECPPLIB CurlResponse
{
public:
	CurlResponse();
	CurlResponse(int responseCode, const std::string& headers);
	virtual ~CurlResponse();

	virtual void setResponseCode(int responseCode);
	int responseCode() const;

	virtual void setHeaders(const std::string& headers);
	const std::string& headers() const;
	const std::map<std::string, std::string>& headersMap() const;

	bool success() const;
	HttpResponseCategory responseCategory() const;

private:
	int _responseCode;
	std::string _headers;
	std::map<std::string, std::string> _headersMap;

	void updateHeadersMap();
};


struct AZURECPPLIB CurlResponseData : CurlResponse
{
public:
	CurlResponseData();
	CurlResponseData(int responseCode, const std::string& headers, const std::string& data);

	void setData(const std::string& data);
	const std::string& data() const;

private:
	std::string _data;
};


struct AZURECPPLIB CurlHeaderHelper
{
	CurlHeaderHelper();
	CurlHeaderHelper(const CurlHeaderHelper&) = delete;
	CurlHeaderHelper(CurlHeaderHelper&&) = delete;
	~CurlHeaderHelper();

	curl_slist* headers;
};


class AZURECPPLIB CurlWrapper
{
public:
	CurlWrapper();

	void setUrl(const std::string& url);
	const std::string& url() const;

	void addParam(const std::string& param, const std::string& value);
	void clearParams();

	void addHeader(const std::string& header, const std::string& value);
	void clearHeaders();

	//! @brief Resets the headers and params.
	virtual void reset();

	void setFollowRedirect(bool on);
	bool followRedirect() const;

	void setVerbose(bool on);
	bool verbose() const;

    void setUseHttp2(bool http2);
    bool useHttp2() const;

	virtual CurlResponseData get() const;
	virtual CurlResponse getFile(const std::string& filename) const;

    virtual CurlResponseData put(const std::string& data) const;
    virtual CurlResponseData put(const void* buffer, size_t size) const;
	virtual CurlResponseData putFile(const std::string& filename);
	
	//virtual CurlResponseData post(const std::string& postData) const;
	//virtual CurlResponseData post(const void* buffer, size_t size) const;

	virtual CurlResponseData delete_() const;

    //virtual CurlResponseData multipartPost(const std::map<std::string, std::string>& data, 
    //    const std::map<std::string, std::string>& files) const;

	static void initCurl();
	static void cleanupCurl();

	static const std::string CONTENT_TYPE_HEADER;
	static const std::string CONTENT_ENCODING_HEADER;
	static const std::string CONTENT_LENGTH_HEADER;
	static const std::string CONTENT_MD5_HEADER;
	static const std::string CONTENT_LANGUAGE_HEADER;
	static const std::string DATE_HEADER;
	static const std::string IF_MODIFIED_SINCE_HEADER;
	static const std::string IF_MATCH_HEADER;
	static const std::string IF_NONE_MATCH_HEADER;
	static const std::string IF_UNMODIFIED_SINCE_HEADER;
	static const std::string RANGE_HEADER;
	static const std::string ACCEPT_HEADER;

	static std::string urlEncode(const std::string& s);
	static std::string urlDecode(const std::string& s);

    static void setCertDir(const std::string& certDir);
    static void setCertInfo(const std::string& certInfo);

    static void setDefaultVerbosity(bool verbose);
    static bool defaultVerbosity();

    static void setDefaultUseHttp2(bool useHttp2);
    static bool defaultUseHttp2();

protected:
	void populateCurlHeaders(curl_slist*& curlHeaders) const;
	std::string encodedParams() const;
	std::string completeUrl() const;

    void setDefaultParams(CURL* handle) const;

	static const std::string PARAMS_DELIM;
	static const std::string KEY_VALUE_DELIM;
	static const std::string PARAMS_SEPARATOR;

	std::vector<std::pair<std::string, std::string>> _headers;
	std::vector<std::pair<std::string, std::string>> _params;

private:
	static size_t readStringCallback(char * contents, size_t size, size_t nmemb, void * userp);
	static size_t writeStringCallback(void* contents, size_t size, size_t nmemb, void* userp);

    static size_t readBuffCallback(char* contents, size_t size, size_t nmemb, void* userp);
	
	static size_t readFileCallback(char * contents, size_t size, size_t nmemb, void * userp);
	static size_t writeFileCallback(void* contents, size_t size, size_t nmemb, void* userp);

    static std::string certDir;
    static std::string certInfo;

	std::string _url;
	bool _followRedirect;
	bool _verbose;
    bool _useHttp2;

	int64_t _maxUploadSpeed;
	int64_t _maxDownloadSpeed;

    static bool _defaultVerbosity;
    static bool _defaultUseHttp2;

	static std::string joinStrings(const std::vector<std::string>& strings, const std::string& delim);
	
	static void validateCode(CURL* handle, CURLcode code);
	static void validateHandle(CURL* handle);
};
