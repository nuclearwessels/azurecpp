
#pragma once

#include <exception>
#include <unordered_map>
#include <optional>
#include <string>

#include <tinyxml2.h>

using namespace std::string_literals;


namespace azurecpp
{
    class AzureException : public std::exception
    {
    public:
        AzureException();
        AzureException(const std::string& msg);

        const char* what() const override;

    protected:
        std::string _what;
    };


    class AzureResponseBase
    {
    public:
        AzureResponseBase();

        const std::string& prefix() const;
        const std::string& marker() const;
        const std::string& nextMarker() const;
        const std::optional<int>& maxResults() const;

        virtual void parseXML(tinyxml2::XMLElement* enumerationResultsElement);

    private:
        std::string _prefix;
        std::string _marker;
        std::string _nextMarker;
        std::optional<int> _maxResults;
    };


    enum class BlobType
    {
        BLOCK_BLOB,
        PAGE_BLOB,
        APPEND_BLOB
    };


    const std::unordered_map<BlobType, std::string> BLOB_TYPE_MAP_STRINGS =
    {
        { BlobType::BLOCK_BLOB,  "BlockBlob"  },
        { BlobType::PAGE_BLOB,   "PageBlob"   },
        { BlobType::APPEND_BLOB, "AppendBlob" }
    };

    enum class AccessTier
    {
        HOT,
        COOL,
        ARCHIVE
    };


    const std::unordered_map<AccessTier, std::string> ACCESS_TIER_MAP_STRINGS =
    {
        { AccessTier::HOT,     "Hot"     },
        { AccessTier::COOL,    "Cool"    },
        { AccessTier::ARCHIVE, "Archive" }
    };

    enum class LeaseStatus
    {
        UNLOCKED,
        LOCKED
    };

    const std::unordered_map<LeaseStatus, std::string> LEASE_STATUS_MAP_STRINGS =
    {
        { LeaseStatus::UNLOCKED, "unlocked" },
        { LeaseStatus::LOCKED,   "locked"   }
    };

    enum class LeaseState
    {
        AVAILABLE,
        LEASED,
        EXPIRED,
        BREAKING,
        BROKEN
    };


    const std::unordered_map<LeaseState, std::string> LEASE_STATE_MAP_STRINGS =
    {
        { LeaseState::AVAILABLE, "available"s },
        { LeaseState::LEASED,    "leased"s    },
        { LeaseState::EXPIRED,   "expired"s   },
        { LeaseState::BREAKING,  "breaking"s  },
        { LeaseState::BROKEN,    "broken"s    }
    };


    enum class LeaseDuration
    {
        INF,
        FIXED
    };

    const std::unordered_map<LeaseDuration, std::string> LEASE_DURATION_MAP_STRINGS =
    {
        { LeaseDuration::INF,   "infinite" },
        { LeaseDuration::FIXED, "fixed"    }
    };


    enum class CopyStatus
    {
        PENDING,
        SUCCESS,
        ABORTED,
        FAILED
    };

    const std::unordered_map<CopyStatus, std::string> COPY_STATUS_MAP_STRINGS =
    {
        { CopyStatus::PENDING, "pending"s },
        { CopyStatus::SUCCESS, "success"s },
        { CopyStatus::ABORTED, "aborted"s },
        { CopyStatus::FAILED,  "failed"s  }
    };


    const std::unordered_map<bool, std::string> BOOLEAN_MAP_STRINGS =
    {
        { true, "true"   },
        { false, "false" }
    };

}

