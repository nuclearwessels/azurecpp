
#include "azure_cpp_core.h"
#include "xml_parse_utils.h"

using namespace azurecpp;



AzureException::AzureException() :
    _what("AzureException")
{

}


AzureException::AzureException(const std::string& msg)
{
    _what = "AzureException: " + msg;
}


const char* AzureException::what() const
{
    return _what.c_str();
}




AzureResponseBase::AzureResponseBase()
{

}


const std::string& AzureResponseBase::prefix() const
{
    return _prefix;
}

const std::string& AzureResponseBase::marker() const
{
    return _marker;
}

const std::string& AzureResponseBase::nextMarker() const
{
    return _nextMarker;
}

const std::optional<int>& AzureResponseBase::maxResults() const
{
    return _maxResults;
}



void AzureResponseBase::parseXML(tinyxml2::XMLElement* enumerationResultsElement)
{
    validateName(enumerationResultsElement, "EnumerationResults");

    _marker = readString(enumerationResultsElement, "Marker");
    _maxResults = readInt(enumerationResultsElement, "MaxResults");
    _nextMarker = readString(enumerationResultsElement, "NextMarker");
    _prefix = readString(enumerationResultsElement, "Prefix");
}

