

#include "azure_blob.h"
#include "string_utils.h"
#include "base64.h"
#include "hmac_sha2.h"
#include "string_utils.h"
#include "tinyxml2.h"
#include "xml_parse_utils.h"
#include "utils.h"
#include <ctime>
#include <sstream>

using namespace azurecpp;

using namespace std::string_literals;


SASToken::SASToken(const std::string& sasToken) :
    _sasToken(sasToken)
{
    std::vector<std::string> parts = split(sasToken, "&?");

    for (const std::string& part : parts)
    {
        std::vector<std::string> kvp = split(part, "=");

        if (kvp.size() >= 2)
        {
            _parts[kvp.at(0)] = kvp.at(1);
        }
    }
}


const std::string& SASToken::sasToken() const
{
    return _sasToken;
}


std::string SASToken::value(const std::string& part) const
{

    if (_parts.find(part) != _parts.cend())
    {
        return _parts.at(part);
    }

    return std::string();
}

const std::map<std::string, std::string>& SASToken::parts() const
{
    return _parts;
}





GetBlobResponse::GetBlobResponse() :
    CurlResponseData()
{

}

GetBlobResponse::GetBlobResponse(const CurlResponseData& crd) :
    CurlResponseData(crd)
{
    updateFields();
}

GetBlobResponse::GetBlobResponse(CurlResponseData&& crd) :
    CurlResponseData(crd)
{
    updateFields();
}


void GetBlobResponse::setHeaders(const std::string& headers)
{
    CurlResponseData::setHeaders(headers);

    updateFields();
}


void GetBlobResponse::updateFields()
{
    const std::map<std::string, std::string>& map = headersMap();

    // handle the metadata
    static const std::string METADATA_PREFIX = "x-ms-meta-"s;

    _metadata.clear();

    for (const std::pair<std::string, std::string>& p : map)
    {
        if (beginsWith(p.first, METADATA_PREFIX))
        {
            std::string name = p.first.substr(METADATA_PREFIX.size());

            _metadata.insert(std::make_pair(name, p.second));
        }
    }

    _creationTime = parseDate(map.at("x-ms-creation-time"s));

    _contentLength = std::stoll(map.at("Content-Length"s));

    _blobType = findValue(BLOB_TYPE_MAP_STRINGS, map.at("x-ms-blob-type"s));
    
    auto it = map.find("x-ms-blob-sequence-number"s);
    if (it != map.cend())
    {
        _sequenceNumber = std::stoll(it->second);
    }

    it = map.find("x-ms-copy-completion-time"s);
    if (it != map.cend())
    {
        _copyCompletionTime = parseDate(it->second);
    }

    updateField(map, "Content-Type"s, _contentType);
    updateField(map, "Content-Range"s, _contentRange);
    updateField(map, "ETag"s, _etag);
    updateField(map, "Content-MD5"s, _contentMD5);
    updateField(map, "x-ms-content-crc64"s, _contentCRC64);
    updateField(map, "Content-Encoding"s, _contentEncoding);
    updateField(map, "Content-Language"s, _contentLanguage);
    updateField(map, "Cache-Control"s, _cacheControl);
    updateField(map, "Content-Disposition"s, _contentDisposition);

    updateField(map, "x-ms-copy-status-description"s, _copyStatusDescription);
    updateField(map, "x-ms-copy-id"s, _copyId);
    updateField(map, "x=ms-copy-progress"s, _copyProgress);
    updateField(map, "x-ms-copy-source"s, _copySource);
    

    it = map.find("x-ms-copy-status"s);
    if (it != map.cend())
    {
        _copyStatus = findValue(COPY_STATUS_MAP_STRINGS, map.at("x-ms-copy-status"s));
    }
    
    it = map.find("x-ms-lease-duration"s);
    if (it != map.cend())
    {
        _leaseDuration = findValue(LEASE_DURATION_MAP_STRINGS, map.at("x-ms-lease-duration"s));
    }

    it = map.find("x-ms-lease-state"s);
    if (it != map.cend())
    {
        _leaseState = findValue(LEASE_STATE_MAP_STRINGS, map.at("x-ms-lease-state"s));
    }

    it = map.find("x-ms-lease-status"s);
    if (it != map.cend())
    {
        _leaseStatus = findValue(LEASE_STATUS_MAP_STRINGS, map.at("x-ms-lease-status"s));
    }

    updateField(map, "x-ms-request-id"s, _requestId);
    updateField(map, "x-ms-version"s, _version);
    updateField(map, "Accept-Ranges"s, _acceptRanges);
    
    _date = parseDate(map.at("Date"s));

    updateField(map, "Access-Control-Allow-Origin", _accessControlAllowOrigin);
    updateField(map, "Access-Control-Allow-Credentials"s, _accessControlAllowCredentials);
    updateField(map, "Access-Control-Expose-Headers", _accessControlExposeHeaders);
    updateField(map, "Vary", _vary);

    it = map.find("x-ms-blob-committed-block-count"s);
    if (it != map.cend())
    {
        _committedBlockCount = std::stoll(it->second);
    }
    
    it = map.find("x-ms-server-encrypted"s);
    if (it != map.cend())
    {
        _serverEncrypted = findValue(BOOLEAN_MAP_STRINGS, it->second);
    }

    updateField(map, "x-ms-encryption-key-sha256"s, _encryptionKeySHA256);
    updateField(map, "x-ms-encryption-scope"s, _encryptionScope);
    updateField(map, "x-ms-blob-content-md5"s, _blobContentMD5);
    updateField(map, "x-client-request-id"s, _clientRequestId);

    it = map.find("x-ms-last-access-time"s);
    if (it != map.cend())
    {
        _lastAccessTime = parseDate(it->second);
    }

    updateField(map, "x-ms-blob-sealed"s, _blobSealed);
}


void GetBlobResponse::updateField(const std::map<std::string, std::string>& map, const std::string& header, std::string& field)
{
    auto search = map.find(header);
    if (search != map.cend())
    {
        field = search->second;
    }
}



// *****************************************************************************************************


const std::string BlobService::API_VERSION = "2021-12-02";
const std::string BlobService::EOL = "\n";

int BlobService::_defaultConnErrRetries = 0;


void BlobService::setDefaultConnErrRetries(int retries)
{
    _defaultConnErrRetries = retries;
}


int BlobService::defaultConnErrRetries()
{
    return _defaultConnErrRetries;
}



BlobService::BlobService() :
    _http(false)
{

}


BlobService::BlobService(const std::string& account) :
    _account(account),
    _http(false)
{

}


void BlobService::setSASToken(const std::string& sasToken)
{
    _sasToken = sasToken;
}

const std::string& BlobService::sasToken() const
{
    return _sasToken;
}

void BlobService::setAccountKey(const std::string& accountKey)
{
    _accountKey = accountKey;
}


const std::string& BlobService::accountKey() const
{
    return _accountKey;
}


void BlobService::setAccount(const std::string& account)
{
    _account = account;
}

const std::string& BlobService::account() const
{
    return _account;
}


void BlobService::clear()
{
    CurlWrapper::reset();

    _account.clear();
    _sasToken.clear();
    _accountKey.clear();
}


CurlResponseData BlobService::get() const
{
    int retriesRemaining = defaultConnErrRetries();

    CurlResponseData crd;

    bool done = false;
    while (!done)
    {
        try 
        {
            crd = CurlWrapper::get();
            done = true;
        }
        catch (const CurlCodeException& cce)
        {
            if (cce.curlCode() == CURLE_RECV_ERROR)
            {
                if (retriesRemaining)
                {
                    retriesRemaining--;
                }
                else
                {
                    throw;
                }
            }
            else
            {
                throw;
            }

        }
    }

    return crd;
}


CurlResponseData BlobService::put(const std::string& data) const
{
    bool done = false;

    int retriesRemaining = defaultConnErrRetries();

    CurlResponseData crd;

    while (!done)
    {
        try
        {
            crd = CurlWrapper::put(data);
            done = true;
        }
        catch (const CurlCodeException& cce)
        {
            if (cce.curlCode() == CURLE_RECV_ERROR)
            {
                if (retriesRemaining)
                {
                    retriesRemaining--;
                }
                else
                {
                    throw;
                }
            }
            else
            {
                throw;
            }
        }
    }

    return crd;
}


CurlResponseData BlobService::put(const void* buffer, size_t size) const
{
    bool done = false;

    int retriesRemaining = defaultConnErrRetries();

    CurlResponseData crd;

    while (!done)
    {
        try
        {
            crd = CurlWrapper::put(buffer, size);
            done = true;
        }
        catch (const CurlCodeException& cce)
        {
            if (cce.curlCode() == CURLE_RECV_ERROR)
            {
                if (retriesRemaining)
                {
                    retriesRemaining--;
                }
                else
                {
                    throw;
                }
            }
            else
            {
                throw;
            }
        }
    }

    return crd;
}






ListContainerResponse BlobService::listContainers()
{
    reset();

    std::string url = "https://" + account() + ".blob.core.windows.net";

    setUrl(url);

    addHeader("x-ms-date", currentDateTime());
    addHeader("x-ms-version", API_VERSION);

    addParam("comp", "list");

    addSecurityInfo("GET", "");

    CurlResponseData crd = get();

    if (crd.responseCategory() != HttpResponseCategory::SUCCESS)
    {
        throw AzureException("listContainers failed.  Response: " + crd.data());
    }

    tinyxml2::XMLDocument doc;
    doc.Parse(crd.data().c_str());

    ListContainerResponse lcr;
    lcr.parseXML(doc);

    return lcr;
}



ListBlobResponse BlobService::listBlobs(const std::string& container, const ListBlobOptions& options)
{
    reset();

    std::string url = "https://" + account() + ".blob.core.windows.net/" + container;
    setUrl(url);

    addHeader("x-ms-date", currentDateTime());
    addHeader("x-ms-version", API_VERSION);


    addParam("restype", "container");
    addParam("comp", "list");

    if (!options.prefix.empty())
    {
        addParam("prefix", options.prefix);
    }

    if (!options.delimiter.empty())
    {
        addParam("delimiter", options.delimiter);
    }

    if (!options.marker.empty())
    {
        addParam("marker", options.marker);
    }

    if (options.maxResults.has_value())
    {
        addParam("maxresults", std::to_string(options.maxResults.value()));
    }

    std::string includeOpts = options.includeOptions.str();
    if (!includeOpts.empty())
    {
        addParam("include", includeOpts);
    }

    std::string showOnlyOpts = options.showOnlyOptions.str();
    if (!showOnlyOpts.empty())
    {
        addParam("showonly", showOnlyOpts);
    }

    if (options.timeout.has_value())
    {
        addParam("timeout", std::to_string(options.timeout.value()));
    }

    addSecurityInfo("GET", container);

    CurlResponseData crd = get();

    if (!crd.success())
    {
        std::string msg = "List blobs on " + container + " failed";
        throw AzureException(msg);
    }

    tinyxml2::XMLDocument doc;
    doc.Parse(crd.data().c_str());

    ListBlobResponse lbr;
    lbr.parseXML(doc);

    return lbr;
}



GetBlobResponse BlobService::getBlob(const std::string& container,
    const std::string& blobName,
    const GetBlobOptions& options)
{
    reset();

    std::string url = "https://" + account() + ".blob.core.windows.net/" + container +
        "/" + encodeResource(blobName);
    setUrl(url);

    addHeader("x-ms-date", currentDateTime());
    addHeader("x-ms-version", API_VERSION);

    options.update(*this);

    addSecurityInfo("GET", container + "/" + encodeResource(blobName));

    GetBlobResponse gbr = get();

    if (!gbr.success())
    {
        std::string msg = "Get blob failed on " + container + "/" + blobName + " failed.  Response: " + gbr.data();
        throw AzureException(msg);
    }

    return gbr;
}




void BlobService::putBlob(const std::string& container, const std::string& blobName,
    const PutBlobOptions& options,
    const void* buffer, int buflen)
{
    reset();

    std::string url = "https://" + account() + ".blob.core.windows.net/" + urlEncode(container) +
        "/" + encodeResource(blobName);
    setUrl(url);

    addHeader("x-ms-date", currentDateTime());
    addHeader("x-ms-version", API_VERSION);

    addHeader("Content-Length", std::to_string(buflen));

    options.update(*this);

    addSecurityInfo("PUT", container + "/" + encodeResource(blobName));

    CurlResponseData crd = put(buffer, buflen);

    if (!crd.success())
    {
        std::string msg = "Put blob to " + container + "/" + blobName + " failed";
        throw AzureException(msg);
    }
}


void BlobService::putBlock(const std::string& container, const std::string& blobName,
    const std::string& blockId,
    const PutBlockOptions& options,
    const void* buffer, int buflen)
{
    reset();

    std::string url = "https://" + account() + ".blob.core.windows.net/" + urlEncode(container) +
        "/" + encodeResource(blobName);
    setUrl(url);

    addParam("blockid", blockId);
    addParam("comp", "block");

    addHeader("x-ms-date", currentDateTime());
    addHeader("x-ms-version", API_VERSION);

    addHeader("Content-Length", std::to_string(buflen));


    if (!options.contentCRC64.empty())
    {
        addHeader("x-ms-content-crc64", options.contentCRC64);
    }

    if (!options.encryptionScope.empty())
    {
        addHeader("x-ms-encryption-scope", options.encryptionScope);
    }

    if (!options.clientRequestId.empty())
    {
        addHeader("x-ms-client-request-id", options.clientRequestId);
    }

    if (options.timeout.has_value())
    {
        addParam("timeout", std::to_string(options.timeout.value()));
    }

    if (!options.leaseId.empty())
    {
        addHeader("x-ms-lease-id", options.leaseId);
    }

    addSecurityInfo("PUT", container + "/" + encodeResource(blobName));

    CurlResponseData crd = put(buffer, buflen);;

    if (!crd.success())
    {
        std::string msg = "Put block to " + container + "/" + blobName + " failed";
        throw AzureException(msg);
    }

}


void BlobService::putBlockList(const std::string& container, const std::string& blobName,
    const PutBlockListOptions& options,
    const std::vector<PutBlockBlob>& blockIds)
{
    reset();

    std::string url = "https://" + account() + ".blob.core.windows.net/" + urlEncode(container) +
        "/" + encodeResource(blobName);
    setUrl(url);

    addParam("comp", "blocklist");

    tinyxml2::XMLDocument doc;

    tinyxml2::XMLElement* blockListElement = doc.NewElement("BlockList");

    doc.InsertFirstChild(blockListElement);

    for (const PutBlockBlob& pbb : blockIds)
    {
        tinyxml2::XMLElement* blockElement = pbb.element(doc);
        blockListElement->InsertEndChild(blockElement);
    }

    tinyxml2::XMLPrinter printer(nullptr, false);
    doc.Print(&printer);

    std::string sendme;
    sendme += printer.CStr();

    addHeader("x-ms-date", currentDateTime());
    addHeader("x-ms-version", API_VERSION);

    addHeader("Content-Length", std::to_string(sendme.size()));
    addHeader("Content-Type", "application/xml");

    options.update(*this);

    addSecurityInfo("PUT", container + "/" + encodeResource(blobName));

    CurlResponseData crd = put(sendme.c_str(), sendme.size());

    if (!crd.success())
    {
        std::string msg = "Put blob to " + container + "/" + blobName + " failed.  Response: " + crd.data();
        throw AzureException(msg);
    }
}



bool BlobService::blobExists(const std::string& container, const std::string& blobName)
{
    ListBlobOptions options;
    options.prefix = blobName;

    ListBlobResponse response = listBlobs(container, options);
    return !response.blobs().empty();
}



void BlobService::deleteBlob(const std::string& container, const std::string& blobName, const DeleteBlobOptions& options)
{
    reset();

    std::string url = "https://" + account() + ".blob.core.windows.net/" + urlEncode(container) +
        "/" + encodeResource(blobName);
    setUrl(url);

    addHeader("x-ms-date", currentDateTime());
    addHeader("x-ms-version", API_VERSION);

    options.update(*this);

    addSecurityInfo("DELETE", container + "/" + encodeResource(blobName));

    CurlResponseData crd = delete_();

    if (!crd.success())
    {
        std::string msg = "Delete blob " + container + "/" + blobName + " failed.  Response: " + crd.data();
        throw AzureException(msg);
    }
}


void BlobService::setBlobTags(const std::string& container, const std::string& blobName, const std::map<std::string, std::string>& tags)
{
    reset();

    tinyxml2::XMLDocument doc;
    doc.SetBOM(true);

    tinyxml2::XMLDeclaration* decl = doc.NewDeclaration();
    doc.InsertFirstChild(decl);

    tinyxml2::XMLElement* tagsElement = doc.NewElement("Tags");
    doc.InsertEndChild(tagsElement);

    tinyxml2::XMLElement* tagSetElement = doc.NewElement("TagSet");
    tagsElement->InsertEndChild(tagSetElement);


    for (const auto& tag : tags)
    {
        tinyxml2::XMLElement* tagElement = doc.NewElement("Tag");
        
        tinyxml2::XMLElement* keyElement = doc.NewElement("Key");
        keyElement->SetText(tag.first.c_str());

        tinyxml2::XMLElement* valueElement = doc.NewElement("Value");
        valueElement->SetText(tag.second.c_str());

        tagElement->InsertEndChild(keyElement);
        tagElement->InsertEndChild(valueElement);

        tagSetElement->InsertEndChild(tagElement);
    }

    tinyxml2::XMLPrinter printer(nullptr, true);
    doc.Print(&printer);

    std::string tagData(printer.CStr());


    std::string url = "https://" + account() + ".blob.core.windows.net/" + urlEncode(container) +
        "/" + encodeResource(blobName);
    setUrl(url);

    addParam("comp", "tags");

    addHeader("x-ms-date", currentDateTime());
    addHeader("x-ms-version", API_VERSION);

    addHeader("Content-Length", std::to_string(tagData.size()));
    addHeader("Content-Type", "application/xml; charset=UTF-8");

    addSecurityInfo("PUT", container + "/" + encodeResource(blobName));

    CurlResponseData crd = put(tagData);

    if (!crd.success())
    {
        std::string msg = "SetBlobTags blob " + container + "/" + blobName + " failed.  Response: " + crd.data();
        throw AzureException(msg);
    }

}


std::map<std::string, std::string> BlobService::getBlobTags(const std::string& container, const std::string& blobName)
{
    reset();

    std::string url = "https://" + account() + ".blob.core.windows.net/" + urlEncode(container) +
        "/" + encodeResource(blobName);
    setUrl(url);

    addParam("comp", "tags");

    addHeader("x-ms-date", currentDateTime());
    addHeader("x-ms-version", API_VERSION);

    addSecurityInfo("GET", container + "/" + encodeResource(blobName));

    CurlResponseData crd = get();

    tinyxml2::XMLDocument doc;
    doc.Parse(crd.data().c_str());

    tinyxml2::XMLElement* tagsElement = doc.FirstChildElement("Tags");

    tinyxml2::XMLElement* tagSetElement = tagsElement->FirstChildElement("TagSet");

    tinyxml2::XMLElement* tagElement = tagSetElement->FirstChildElement("Tag");

    std::map<std::string, std::string> tagMap;

    while (tagElement != nullptr)
    {
        tinyxml2::XMLElement* keyElement = tagElement->FirstChildElement("Key");
        tinyxml2::XMLElement* valueElement = tagElement->FirstChildElement("Value");

        std::string key = keyElement->GetText();
        std::string value = valueElement->GetText();

        tagMap.insert(std::make_pair(key, value) );

        tagElement = tagElement->NextSiblingElement();
    }

    return tagMap;
}



void BlobService::setOptions(const PutBlobOptions& options, bool includeBlobType)
{
    if (includeBlobType)
    {
        addHeader("x-ms-blob-type", BLOB_TYPE_MAP_STRINGS.at(options.blobType));
    }


}



void BlobService::addSecurityInfo(const std::string& verb, const std::string& resource)
{
    if (accountKey().size())
    {
        std::string sigBytes = base64_decode(accountKey());

        std::string sts = stringToSignAccountKey(verb, resource);

        unsigned char mac[32];
        memset(mac, 0, 32);

        hmac_sha256((const unsigned char*)sigBytes.c_str(), (int)sigBytes.size(),
            (const unsigned char*)sts.c_str(), (int)sts.size(),
            mac, 32);

        std::string macString((char*)mac, 32);
        std::string macStringBase64 = base64_encode(macString);

        std::string authorization = "SharedKey " + account() + ":" + macStringBase64;
        addHeader("Authorization", authorization);
    }
    else if (sasToken().size())
    {
        SASToken st(sasToken());

        for (const auto& part : st.parts())
        {
            std::string decodedValue = urlDecode(part.second);
            addParam(part.first, decodedValue);
        }
    }
    else
    {
        throw std::exception("Not implemented");
    }

}


std::string BlobService::stringToSignAccountKey(const std::string& verb, const std::string& resource) const
{
    static const std::vector<std::string> HEADERS =
    {
        CONTENT_ENCODING_HEADER,
        CONTENT_LANGUAGE_HEADER,
        CONTENT_LENGTH_HEADER,
        CONTENT_MD5_HEADER,
        CONTENT_TYPE_HEADER,
        DATE_HEADER,
        IF_MODIFIED_SINCE_HEADER,
        IF_MATCH_HEADER,
        IF_NONE_MATCH_HEADER,
        IF_UNMODIFIED_SINCE_HEADER,
        RANGE_HEADER
    };

    std::string stringToSign;

    stringToSign += toUpper(verb);
    stringToSign += EOL;

    for (const std::string& header : HEADERS)
    {
        std::string v;

        auto it = std::find_if(_headers.cbegin(), _headers.cend(),
            [&](const auto& pair)
            {
                return pair.first == header;
            });

        if (it != _headers.cend())
        {
            v = it->second;
        }

        stringToSign += v;
        stringToSign += EOL;
    }

    // canonical headers
    std::vector<std::pair<std::string, std::string>> canonicalHeaders;
    for (const auto& header : _headers)
    {
        if (beginsWith(header.first, "x-ms-", false))
        {
            canonicalHeaders.push_back(header);
        }
    }

    // sort the headers
    std::sort(canonicalHeaders.begin(), canonicalHeaders.end(),
        [&](const auto& a, const auto& b)
        {
            std::string keyA = toLower(a.first);
            std::string keyB = toLower(b.first);
            return keyA < keyB;
        });

    std::string canonicalHeaderString;
    for (const auto& ch : canonicalHeaders)
    {
        std::string key = toLower(ch.first);
        std::string s = key + ":" + ch.second + EOL;
        canonicalHeaderString += s;
    }

    stringToSign += canonicalHeaderString;

    // canonical resource
    std::string canonicalResourceString = "/" + account() + "/" + resource;

    std::vector<std::pair<std::string, std::string>> queryParams;
    for (const auto& qp : _params)
    {
        queryParams.push_back(std::make_pair(toLower(qp.first), qp.second));
    }

    std::sort(queryParams.begin(), queryParams.end(),
        [&](const auto& a, const auto& b)
        {
            return a.first < b.first;
        });

    std::vector<std::pair<std::string, std::string>> uniqueQueryParams;

    std::unique_copy(queryParams.cbegin(), queryParams.cend(), std::back_inserter(uniqueQueryParams),
        [&](const auto& a, const auto& b)
        {
            return a.first == b.first;
        });

    std::vector<std::string> canonicalResourceParts;
    for (const auto& uniqueQueryParam : uniqueQueryParams)
    {
        std::string header = uniqueQueryParam.first;

        // now find all of the values with this header
        std::vector<std::string> values;

        for (const auto& qp : queryParams)
        {
            if (qp.first == header)
            {
                values.push_back(qp.second);
            }
        }

        std::sort(values.begin(), values.end());

        canonicalResourceParts.push_back(header + ":" + join(",", values));
    }

    for (const std::string& part : canonicalResourceParts)
    {
        canonicalResourceString += EOL;
        canonicalResourceString += part;
    }

    stringToSign += canonicalResourceString;

    return stringToSign;
}



std::string BlobService::encodeResource(const std::string& resource)
{
    // need to URL encode the parts, but not the slashes (/)
    std::vector<std::string> parts = split(resource, "/");

    std::vector<std::string> v;

    for (const std::string& part : parts)
    {
        v.push_back(urlEncode(part));
    }

    return join("/", v);
}




