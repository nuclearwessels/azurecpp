

#include "xml_parse_utils.h"
#include "azure_cpp_core.h"

#include <ctime>
#include <stdexcept>



void azurecpp::validateName(tinyxml2::XMLElement* element, const std::string& name)
{
    if (name.compare(element->Name()) != 0)
    {
        std::string msg = "Element is not a " + name;
        throw std::invalid_argument(msg);
    }
}


void azurecpp::validateChild(tinyxml2::XMLElement* element, const std::string& child)
{
    tinyxml2::XMLElement* childElement = element->FirstChildElement(child.c_str());
    if (childElement == nullptr)
    {
        throw std::invalid_argument("No child " + child);
    }
}


std::string azurecpp::readString(tinyxml2::XMLElement* element, const std::string& child)
{
    std::string value;

    tinyxml2::XMLElement* childElement = element->FirstChildElement(child.c_str());
    if (childElement != nullptr)
    {
        if (childElement->GetText())
        {
            value = childElement->GetText();
        }
    }

    return value;
}


time_t azurecpp::parseDate(const std::string& date)
{
    static const std::map<std::string, int> MONTH_MAP =
    {
        { "Jan", 1 },
        { "Feb", 2 },
        { "Mar", 3 },
        { "Apr", 4 },
        { "May", 5 },
        { "Jun", 6 },
        { "Jul", 7 },
        { "Aug", 8 },
        { "Sep", 9 },
        { "Oct", 10 },
        { "Nov", 11 },
        { "Dec", 12 }
    };

    int day, year;

    char dow[10];
    char month[10];
    char tz[10];

    memset(dow, 0, 10);
    memset(month, 0, 10);
    memset(tz, 0, 10);

    int hour, minute, second;

    constexpr int NUM_FIELDS = 8;

    int parsed = sscanf(date.c_str(), "%s %d %s %d %d:%d:%d %s",
        dow, &day, month, &year, &hour, &minute, &second, tz);

    if (parsed != NUM_FIELDS)
    {
        throw std::logic_error("Unable to parse date value");
    }

    tm tm;
    memset(&tm, 0, sizeof(tm));

    tm.tm_year = year - 1900;
    tm.tm_mday = day;
    tm.tm_mon = MONTH_MAP.at(std::string(month)) - 1;
    tm.tm_hour = hour;
    tm.tm_min = minute;
    tm.tm_sec = second;

    time_t date2 = mktime(&tm);
    
    return date2;
}


std::optional<time_t> azurecpp::readDate(tinyxml2::XMLElement* element, const std::string& child)
{
    std::optional<time_t> date;

    tinyxml2::XMLElement* childElement = element->FirstChildElement(child.c_str());
    if (childElement)
    {
        std::string temp(childElement->GetText());
        date = parseDate(temp);
    }

    return date;
}


std::optional<bool> azurecpp::readBool(tinyxml2::XMLElement* element, const std::string& child)
{
    std::optional<bool> value;

    tinyxml2::XMLElement* childElement = element->FirstChildElement(child.c_str());
    if (childElement)
    {
        if (strcmp(childElement->GetText(), "true") == 0)
        {
            value = true;
        }
        else if (strcmp(childElement->GetText(), "false") == 0)
        {
            value = false;
        }
        else
        {
            std::string msg = "Unable to parse boolean: " + std::string(childElement->GetText());
            throw AzureException(msg);
        }
    }

    return value;
}



std::optional<int> azurecpp::readInt(tinyxml2::XMLElement* element, const std::string& child)
{
    std::optional<int> value;

    tinyxml2::XMLElement* childElement = element->FirstChildElement(child.c_str());
    if (childElement)
    {
        std::string s(childElement->GetText());
        value = std::stoi(s);
    }

    return value;
}


std::optional<long long> azurecpp::readLong(tinyxml2::XMLElement* element, const std::string& child)
{
    std::optional<long long> value;

    tinyxml2::XMLElement* childElement = element->FirstChildElement(child.c_str());
    if (childElement)
    {
        std::string s(childElement->GetText());
        value = std::stoll(s);
    }

    return value;
}




std::map<std::string, std::string> azurecpp::readMetadata(tinyxml2::XMLElement* element, const std::string& child)
{
    std::map<std::string, std::string> metadata;

    tinyxml2::XMLElement* metadataElement = element->FirstChildElement(child.c_str());
    if (metadataElement)
    {
        for (tinyxml2::XMLElement* child = metadataElement->FirstChildElement(); child != nullptr; child = child->NextSiblingElement())
        {
            std::string name(child->Name());
            std::string text(child->GetText());

            metadata.insert(std::pair(name, text));
        }
    }

    return metadata;
}


std::map<std::string, std::string> azurecpp::readTags(tinyxml2::XMLElement* tagsElement, const std::string& child)
{
    std::map<std::string, std::string> tags;

    tinyxml2::XMLElement* tagSetElement = tagsElement->FirstChildElement("TagSet");
    if (tagSetElement)
    {
        for (tinyxml2::XMLElement* tagElement = tagSetElement->FirstChildElement("Tag"); tagElement != nullptr; tagElement = tagElement->NextSiblingElement())
        {
            tinyxml2::XMLElement* keyElement = tagElement->FirstChildElement("Key");
            tinyxml2::XMLElement* valueElement = tagElement->FirstChildElement("Value");

            if (keyElement && valueElement)
            {
                std::string key(keyElement->GetText());
                std::string value(valueElement->GetText());

                tags.insert(std::pair(key, value));
            }
        }
    }

    return tags;
}
