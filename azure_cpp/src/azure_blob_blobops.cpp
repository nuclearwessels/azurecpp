
#include "azure_blob_blobops.h"
#include "utils.h"

using namespace azurecpp;



CommonBlobOptionsBase::CommonBlobOptionsBase()
{

}




void CommonBlobOptionsBase::update(CurlWrapper& wrapper) const
{
    if (!clientRequestId.empty())
    {
        wrapper.addHeader("x-ms-client-request-id", clientRequestId);
    }

    if (!leaseId.empty())
    {
        wrapper.addHeader("x-ms-lease-id", leaseId);
    }

    if (timeout.has_value())
    {
        wrapper.addParam("timeout", std::to_string(timeout.value()));
    }

}




CommonBlobOptions::CommonBlobOptions()
{

}



void CommonBlobOptions::update(CurlWrapper& wrapper) const
{
    CommonBlobOptionsBase::update(wrapper);

    if (!origin.empty())
    {
        wrapper.addHeader("Origin", origin);
    }
}



void PutBlobOptionsBase::update(CurlWrapper& wrapper) const
{
    CommonBlobOptions::update(wrapper);

    if (!contentMD5.empty())
    {
        wrapper.addHeader("x-ms-content-md5", contentMD5);
    }

    if (!contentCRC64.empty())
    {
        wrapper.addHeader("x-ms-content-crc64", contentCRC64);
    }

    if (!encryptionScope.empty())
    {
        wrapper.addHeader("x-ms-encryption-scope", encryptionScope);
    }

    for (const auto& p : metadata)
    {
        wrapper.addHeader("x-ms-meta-" + p.first, p.second);
    }

}


PutBlockListOptions::PutBlockListOptions() 
{

}


void PutBlockListOptions::update(CurlWrapper& wrapper) const
{
    PutBlobOptionsBase::update(wrapper);

    if (!contentType.empty())
    {
        wrapper.addHeader("x-ms-blob-content-type", contentType);
    }

    if (!contentEncoding.empty())
    {
        wrapper.addHeader("Content-Encoding", contentEncoding);
    }

    if (!contentLanguage.empty())
    {
        wrapper.addHeader("Content-Language", contentLanguage);
    }

    if (!cacheControl.empty())
    {
        wrapper.addHeader("Cache-Control", cacheControl);
    }

    if (!contentDisposition.empty())
    {
        wrapper.addHeader("x-ms-content-disposition", contentDisposition);
    }

    if (accessTier.has_value())
    {
        wrapper.addHeader("x-ms-access-tier", ACCESS_TIER_MAP_STRINGS.at(accessTier.value()));
    }

    if (timeout.has_value())
    {
        wrapper.addParam("timeout", std::to_string(timeout.value()));
    }
}



PutBlobOptions::PutBlobOptions() :
    blobType(BlobType::BLOCK_BLOB)
{
    
}


void PutBlobOptions::update(CurlWrapper& wrapper) const
{
    PutBlockListOptions::update(wrapper);

    wrapper.addHeader("x-ms-blob-type", BLOB_TYPE_MAP_STRINGS.at(blobType));
}



PutBlockOptions::PutBlockOptions()
{

}



Range::Range() :
    start(0)
{

}


std::string Range::str() const
{
    std::string s("bytes=");

    s += std::to_string(start);
    s += "-";

    if (end.has_value())
    {
        s += std::to_string(end.value());
    }

    return s;
}



PutBlockBlob::PutBlockBlob() :
    blockType(BlockListType::LATEST)
{

}



tinyxml2::XMLElement* PutBlockBlob::element(tinyxml2::XMLDocument& doc) const
{
    static const std::map<BlockListType, std::string> BLOCK_LIST_STRING =
    {
        { BlockListType::LATEST, "Latest" },
        { BlockListType::COMMITTED, "Committed" },
        { BlockListType::UNCOMMITTED, "Uncommitted" }
    };

    tinyxml2::XMLElement* element = doc.NewElement(BLOCK_LIST_STRING.at(blockType).c_str());
    element->SetText(blockId.c_str());

    return element;
}



GetBlobOptions::GetBlobOptions()
{

}


void GetBlobOptions::update(CurlWrapper& wrapper) const
{
    CommonBlobOptions::update(wrapper);

    if (range.has_value())
    {
        wrapper.addHeader("Range", range.value().str());
    }

    if (rangeContentMD5.has_value())
    {
        wrapper.addHeader("x-ms-range-get-content-md5", rangeContentMD5.value() ? "true" : "false");
    }

    if (rangeContentCRC64.has_value())
    {
        wrapper.addHeader("x-ms-range-get-content-crc64", rangeContentCRC64.value() ? "true" : "false");
    }

    if (snapshot.has_value())
    {
        //wrapper.addParam("snapshot", snapshot);
    }
}



const std::map<DeleteSnapshots, std::string> DeleteBlobOptions::DELETE_SNAPSHOTS_MAP_STRINGS =
{
    { DeleteSnapshots::INCLUDE, "include" },
    { DeleteSnapshots::ONLY,    "only"    }
};


const std::map<DeleteType, std::string> DeleteBlobOptions::DELETE_TYPE_MAP_STRINGS =
{
    { DeleteType::PERMANENT, "permanent" }
};


DeleteBlobOptions::DeleteBlobOptions() :
    deleteSnapshots(DeleteSnapshots::INCLUDE)
{

}


void DeleteBlobOptions::update(CurlWrapper& wrapper) const
{
    CommonBlobOptionsBase::update(wrapper);

    if (snapshot.size())
    {
        wrapper.addParam("snapshot", snapshot);
    }
    
    if (versionId.has_value())
    {
        wrapper.addParam("versionid", encodeDateTime(versionId.value()));
    }

    if (deleteType.has_value())
    {
        wrapper.addParam("deletetype", DELETE_TYPE_MAP_STRINGS.at(deleteType.value()));
    }

    wrapper.addHeader("x-ms-delete-snapshots", DELETE_SNAPSHOTS_MAP_STRINGS.at(deleteSnapshots));
}

