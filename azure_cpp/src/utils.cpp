
#include "utils.h"

#include <ctime>


using namespace azurecpp;


std::string azurecpp::encodeDateTime(const time_t& t)
{
    // Date needs to be in this format: Fri, 26 Jun 2015 23:39:12 GMT

    tm tm = *gmtime(&t);
    
    char buf[80];

    strftime(buf, 80, "%a, %d %b %G %H:%M:%S GMT", &tm);

    return std::string(buf);
}


std::string azurecpp::currentDateTime()
{
    time_t now;
    time(&now);

    return encodeDateTime(now);
}

