
#include "azure_blob_list.h"
#include "azure_cpp_core.h"
#include "xml_parse_utils.h"

#include "string_utils.h"


using namespace azurecpp;


ContainerInfo::ContainerInfo()
{

}


void ContainerInfo::parseXML(tinyxml2::XMLElement* element)
{
    validateName(element, "Container");

    _name = readString(element, "Name");

    tinyxml2::XMLElement* propertiesElement = element->FirstChildElement("Properties");
    if (propertiesElement)
    {
        _lastModified = readDate(propertiesElement, "Last-Modified");
        _etag = readString(propertiesElement, "Etag");
        _leaseStatus = readString(propertiesElement, "LeaseStatus");
        _leaseState = readString(propertiesElement, "LeaseState");
        _defaultEncryptionScope = readString(propertiesElement, "DefaultEncryptionScope");
        _denyEncryptionScopeOverride = readBool(propertiesElement, "DenyEncryptionScopeOverride");
        _hasImmutabilityPolicy = readBool(propertiesElement, "HasImmutabilityPolicy");
        _hasLegalHold = readBool(propertiesElement, "HasLegalHold");
    }

    _metadata = readMetadata(element, "Metadata");
}



ListContainerResponse::ListContainerResponse()
{

}

const std::vector<ContainerInfo>& ListContainerResponse::containers() const
{
    return _containers;
}


const std::string& ListContainerResponse::prefix() const
{
    return _prefix;
}


void ListContainerResponse::parseXML(tinyxml2::XMLDocument& doc)
{
    tinyxml2::XMLElement* erElement = doc.FirstChildElement("EnumerationResults");

    AzureResponseBase::parseXML(erElement);

    tinyxml2::XMLElement* containersElement = erElement->FirstChildElement("Containers");
    for (tinyxml2::XMLElement* child = containersElement->FirstChildElement(); child != nullptr; child = child->NextSiblingElement())
    {
        ContainerInfo containerInfo;
        containerInfo.parseXML(child);

        _containers.emplace_back(containerInfo);
    }
}



ListBlobResponse::ListBlobResponse()
{

}


const std::vector<BlobInfo>& ListBlobResponse::blobs() const
{
    return _blobs;
}


void ListBlobResponse::parseXML(tinyxml2::XMLDocument& doc)
{
    tinyxml2::XMLElement* erElement = doc.FirstChildElement("EnumerationResults");

    AzureResponseBase::parseXML(erElement);

    tinyxml2::XMLElement* containersElement = erElement->FirstChildElement("Blobs");
    for (tinyxml2::XMLElement* child = containersElement->FirstChildElement(); child != nullptr; child = child->NextSiblingElement())
    {
        BlobInfo blobInfo;
        blobInfo.parseXML(child);

        _blobs.emplace_back(blobInfo);
    }
}


ListBlobIncludeOptions::ListBlobIncludeOptions() :
    snapshots(false),
    metadata(false),
    uncommittedblobs(false),
    copy(false),
    deleted(false),
    tags(false),
    versions(false)
{

}



std::string ListBlobIncludeOptions::str() const
{
    static const std::vector<std::string> PARTS_STRING =
    {
        "snapshots",
        "metadata",
        "uncommittedblobs",
        "copy",
        "deleted",
        "tags",
        "versions"
    };

    std::vector<bool> parts =
    {
        snapshots,
        metadata,
        uncommittedblobs,
        copy,
        deleted,
        tags,
        versions
    };

    std::vector<std::string> usedParts;

    for (int i = 0; i < parts.size(); i++)
    {
        if (parts.at(i))
        {
            usedParts.push_back(PARTS_STRING.at(i));
        }
    }

    return join(",", usedParts);
}


ListBlobShowOnlyOptions::ListBlobShowOnlyOptions() :
    deleted(false)
{

}



std::string ListBlobShowOnlyOptions::str() const
{
    std::string value;

    if (deleted)
        value += "deleted";

    return value;
}



ListBlobOptions::ListBlobOptions()
{

}


BlobInfo::BlobInfo()
{

}


void BlobInfo::parseXML(tinyxml2::XMLElement* blobElement)
{
    validateName(blobElement, "Blob");

    _name = readString(blobElement, "Name");

    tinyxml2::XMLElement* propElement = blobElement->FirstChildElement("Properties");
    if (propElement)
    {
        _creationTime = readDate(propElement, "Creation-Time").value();
        _lastModifiedTime = readDate(propElement, "Last-Modified").value();
        _etag = readString(propElement, "Etag");
        _contentLength = readLong(propElement, "Content-Length").value();
        _contentType = readString(propElement, "Content-Type");
        _contentEncoding = readString(propElement, "Content-Encoding");
        _contentLanguage = readString(propElement, "Content-Language");
        _contentMD5 = readString(propElement, "Content-MD5");
        _cacheControl = readString(propElement, "Cache-Control");
        _sequenceNumber = readString(propElement, "x-ms-blob-sequence-number");
        _blobType = readMapValue(propElement, "BlobType", BLOB_TYPE_MAP).value();
        _accessTier = readMapValue(propElement, "AccessTier", ACCESS_TIER_MAP);
        _leaseStatus = readMapValue(propElement, "LeaseStatus", LEASE_STATUS_MAP);
        _leaseState = readMapValue(propElement, "LeaseState", LEASE_STATE_MAP);
        _leaseDuration = readMapValue(propElement, "LeaseDuration", LEASE_DURATION_MAP);
        _copyId = readString(propElement, "CopyId");
        _copyStatus = readMapValue(propElement, "CopyStatus", COPY_STATUS_MAP);
        _copySource = readString(propElement, "CopySource");
        _copyProgress = readLong(propElement, "CopyProgress");
        _copyCompletionTime = readDate(propElement, "CopyCompletionTime");
        _copyStatusDesc = readString(propElement, "CopyStatusDescription");
        _serverEncrypted = readBool(propElement, "ServerEncrypted");
        _customerProvidedKeySha256 = readString(propElement, "CustomerProvidedKeySha256");
        _encryptionScope = readString(propElement, "EncryptionScope");
        _incrementalCopy = readBool(propElement, "IncrementalCopy");
        _accessTierInferred = readBool(propElement, "AccessTierInferred");
        _accessTierChangeTime = readDate(propElement, "AccessTierChangeTime");
        _deletedTime = readDate(propElement, "DeletedTime");
        _remainingRetentionDays = readInt(propElement, "RemainingRetentionDays");
        _tagCount = readInt(propElement, "TagCount");
        _rehydratePriority = readString(propElement, "RehydratePriority");
    }

    _metadata = readMetadata(blobElement, "Metadata");
    _tags = readTags(blobElement, "Tags");

}


const std::string& BlobInfo::name() const
{
    return _name;
}

const time_t& BlobInfo::creationTime() const
{
    return _creationTime;
}

const time_t& BlobInfo::lastModifiedTime() const
{
    return _lastModifiedTime;
}

const std::string& BlobInfo::etag() const
{
    return _etag;
}

const long long& BlobInfo::contentLength() const
{
    return _contentLength;
}

const std::string& BlobInfo::contentType() const
{
    return _contentType;
}

const std::string& BlobInfo::contentEncoding() const
{
    return _contentEncoding;
}

const std::string& BlobInfo::contentLanguage() const
{
    return _contentLanguage;
}

const std::string& BlobInfo::contentMD5() const
{
    return _contentMD5;
}

const std::string& BlobInfo::cacheControl() const
{
    return _cacheControl;
}

const std::string& BlobInfo::sequenceNumber() const
{
    return _sequenceNumber;
}

const BlobType& BlobInfo::blobType() const
{
    return _blobType;
}

const std::optional<AccessTier>& BlobInfo::accessTier() const
{
    return _accessTier;
}

const std::optional<LeaseState>& BlobInfo::leaseState() const
{
    return _leaseState;
}

const std::optional<LeaseStatus>& BlobInfo::leaseStatus() const
{
    return _leaseStatus;
}

const std::optional<LeaseDuration>& BlobInfo::leaseDuration() const
{
    return _leaseDuration;
}

const std::string& BlobInfo::copyId() const
{
    return _copyId;
}

const std::optional<CopyStatus>& BlobInfo::copyStatus() const
{
    return _copyStatus;
}

const std::string& BlobInfo::copySource() const
{
    return _copySource;
}

const std::optional<long>& BlobInfo::copyProgress() const
{
    return _copyProgress;
}

const std::optional<time_t>& BlobInfo::copyCompletionTime() const
{
    return _copyCompletionTime;
}

const std::string& BlobInfo::copyStatusDesc() const
{
    return _copyStatusDesc;
}
const std::optional<bool>& BlobInfo::serverEncrypted() const
{
    return _serverEncrypted;
}

const std::string& BlobInfo::customerProvidedKeySha256() const
{
    return _customerProvidedKeySha256;
}

const std::string& BlobInfo::encryptionScope() const
{
    return _encryptionScope;
}

const std::optional<bool>& BlobInfo::incrementalCopy() const
{
    return _incrementalCopy;
}

const std::optional<bool>& BlobInfo::accessTierInferred() const
{
    return _accessTierInferred;
}

const std::optional<time_t>& BlobInfo::accessTierChangeTime() const
{
    return _accessTierChangeTime;
}

const std::optional<time_t>& BlobInfo::deletedTime() const
{
    return _deletedTime;
}

const std::optional<int>& BlobInfo::remainingRetentionDays() const
{
    return _remainingRetentionDays;
}

const std::optional<int>& BlobInfo::tagCount() const
{
    return _tagCount;
}

const std::string& BlobInfo::rehydratePriority() const
{
    return _rehydratePriority;
}

const std::map<std::string, std::string>& BlobInfo::metadata() const
{
    return _metadata;
}

const std::map<std::string, std::string>& BlobInfo::tags() const
{
    return _tags;
}


const std::map<std::string, BlobType> BlobInfo::BLOB_TYPE_MAP =
{
    { "BlockBlob", BlobType::BLOCK_BLOB },
    { "PageBlob", BlobType::PAGE_BLOB },
    { "AppendBlob", BlobType::APPEND_BLOB }
};


const std::map<std::string, AccessTier> BlobInfo::ACCESS_TIER_MAP =
{
    { "Hot", AccessTier::HOT },
    { "Cold", AccessTier::COOL },
    { "Archive", AccessTier::ARCHIVE }
};


const std::map<std::string, LeaseStatus> BlobInfo::LEASE_STATUS_MAP =
{
    { "unlocked", LeaseStatus::UNLOCKED },
    { "locked", LeaseStatus::LOCKED }
};


const std::map<std::string, LeaseState> BlobInfo::LEASE_STATE_MAP =
{
    { "available", LeaseState::AVAILABLE },
    { "leased", LeaseState::LEASED },
    { "expired", LeaseState::EXPIRED },
    { "breaking", LeaseState::BREAKING },
    { "broken", LeaseState::BROKEN }
};


const std::map<std::string, LeaseDuration> BlobInfo::LEASE_DURATION_MAP =
{
    { "infinite", LeaseDuration::INF },
    { "fixed", LeaseDuration::FIXED }
};

const std::map<std::string, CopyStatus> BlobInfo::COPY_STATUS_MAP =
{
    { "pending", CopyStatus::PENDING },
    { "success", CopyStatus::SUCCESS },
    { "aborted", CopyStatus::ABORTED },
    { "failed", CopyStatus::FAILED }
};


