
#pragma once

#include <tinyxml2.h>

#include <map>
#include <unordered_map>
#include <optional>
#include <string>
#include <stdexcept>


namespace azurecpp
{

    time_t parseDate(const std::string& date);

    void validateName(tinyxml2::XMLElement* element, const std::string& name);
    void validateChild(tinyxml2::XMLElement* element, const std::string& child);
    std::string readString(tinyxml2::XMLElement* element, const std::string& child);
    std::optional<time_t> readDate(tinyxml2::XMLElement* element, const std::string& child);
    std::optional<bool> readBool(tinyxml2::XMLElement* element, const std::string& child);
    std::optional<int> readInt(tinyxml2::XMLElement* element, const std::string& child);
    std::optional<long long> readLong(tinyxml2::XMLElement* element, const std::string& child);
    std::map<std::string, std::string> readMetadata(tinyxml2::XMLElement* element, const std::string& child);
    std::map<std::string, std::string> readTags(tinyxml2::XMLElement* tagsElement, const std::string& child);


    template <class T>
    std::optional<T> readMapValue(tinyxml2::XMLElement* element, const std::string& child, const std::map<std::string, T>& map)
    {
        std::optional<T> value;

        std::optional<std::string> svalue = readString(element, child);

        if (svalue.has_value())
        {
            if (!svalue.value().empty())
            {
                value = map.at(svalue.value());
            }
        }

        return value;
    }

    template <class T, class V>
    T findValue(const std::unordered_map<T, V>& map, const V& value)
    {
        auto it = std::find_if(map.cbegin(), map.cend(),
            [&](const std::pair<T, V>& p)
            {
                return p.second == value;
            });

        if (it != map.cend())
        {
            return it->first;
        }
        else
        {
            throw std::logic_error("Value not found: " + value);
        }
    }

}
