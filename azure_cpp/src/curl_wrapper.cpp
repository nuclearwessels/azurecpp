#include "curl_wrapper.h"
#include "curl/curl.h"

#include "string_utils.h"


#include <algorithm>
#include <cassert>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <regex>


struct BufferCallback
{
    char* buffer_;
    int buflen_;
    int bufpos_;
};


CurlException::CurlException() : 
    _what("CurlException")
{

}


CurlException::CurlException(const std::string& msg)
{
    _what = "CurlException: " + msg;
}




CurlCodeException::CurlCodeException(CURLcode curlCode) :
    _curlCode(curlCode)
{
    std::stringstream ss;
    ss << "CurlException: curlCode=" << (int)curlCode << " " << CURL_CODE_MAP.at(curlCode);

    _what = ss.str();
}


CURLcode CurlCodeException::curlCode() const
{
    return _curlCode;
}


const char* CurlException::what() const
{
    return _what.c_str();
}


const std::map<CURLcode, std::string> CurlCodeException::CURL_CODE_MAP =
{
    { CURLE_OK,                         "CURLE_OK"                      },
    { CURLE_UNSUPPORTED_PROTOCOL,       "CURLE_UNSUPPORTED_PROTOCOL"    },
    { CURLE_FAILED_INIT,                "CURLE_FAILED_INIT"             },
    { CURLE_URL_MALFORMAT,              "CURLE_URL_MALFORMAT"           },
    { CURLE_NOT_BUILT_IN,               "CURLE_NOT_BUILT_IN"            },
    { CURLE_COULDNT_RESOLVE_PROXY,      "CURLE_COULDNT_RESOLVE_PROXY"   },
    { CURLE_COULDNT_RESOLVE_HOST,       "CURLE_COULDNT_RESOLVE_HOST"    },
    { CURLE_COULDNT_CONNECT,            "CURLE_COULDNT_CONNECT"         },
    { CURLE_WEIRD_SERVER_REPLY,         "CURLE_WEIRD_SERVER_REPLY"      },
    { CURLE_REMOTE_ACCESS_DENIED,       "CURLE_REMOTE_ACCESS_DENIED"    },
    { CURLE_FTP_ACCEPT_FAILED,          "CURLE_FTP_ACCEPT_FAILED"       },
    { CURLE_FTP_WEIRD_PASS_REPLY,       "CURLE_FTP_WEIRD_PASS_REPLY"    },
    { CURLE_FTP_ACCEPT_TIMEOUT,         "CURLE_FTP_ACCEPT_TIMEOUT"      },
    { CURLE_FTP_WEIRD_PASV_REPLY,       "CURLE_FTP_WEIRD_PASV_REPLY"    },
    { CURLE_FTP_WEIRD_227_FORMAT,       "CURLE_FTP_WEIRD_227_FORMAT"    },
    { CURLE_FTP_CANT_GET_HOST,          "CURLE_FTP_CANT_GET_HOST"       },
    { CURLE_HTTP2,                      "CURLE_HTTP2"                   },
    { CURLE_FTP_COULDNT_SET_TYPE,       "CURLE_FTP_COULDNT_SET_TYPE"    },
    { CURLE_PARTIAL_FILE,               "CURLE_PARTIAL_FILE"            },
    { CURLE_FTP_COULDNT_RETR_FILE,      "CURLE_FTP_COULDNT_RETR_FILE"   },
    { CURLE_OBSOLETE20,                 "CURLE_OBSOLETE20"              },
    { CURLE_QUOTE_ERROR,                "CURLE_QUOTE_ERROR"             },
    { CURLE_HTTP_RETURNED_ERROR,        "CURLE_HTTP_RETURNED_ERROR"     },
    { CURLE_WRITE_ERROR,                "CURLE_WRITE_ERROR"             },
    { CURLE_OBSOLETE24,                 "CURLE_OBSOLETE24"              },
    { CURLE_UPLOAD_FAILED,              "CURLE_UPLOAD_FAILED"           },
    { CURLE_READ_ERROR,                 "CURLE_READ_ERROR"              },
    { CURLE_OUT_OF_MEMORY,              "CURLE_OUT_OF_MEMORY"           },
    { CURLE_OPERATION_TIMEDOUT,         "CURLE_OPERATION_TIMEDOUT"      },
    { CURLE_OBSOLETE29,                 "CURLE_OBSOLETE29"              },
    { CURLE_FTP_PORT_FAILED,            "CURLE_FTP_PORT_FAILED"         },
    { CURLE_FTP_COULDNT_USE_REST,       "CURLE_FTP_COULDNT_USE_REST"    },
    { CURLE_OBSOLETE32,                 "CURLE_OBSOLETE32"              },
    { CURLE_RANGE_ERROR,                "CURLE_RANGE_ERROR"             },
    { CURLE_HTTP_POST_ERROR,            "CURLE_HTTP_POST_ERROR"         },
    { CURLE_SSL_CONNECT_ERROR,          "CURLE_SSL_CONNECT_ERROR"       },
    { CURLE_BAD_DOWNLOAD_RESUME,        "CURLE_BAD_DOWNLOAD_RESUME"     },
    { CURLE_FILE_COULDNT_READ_FILE,     "CURLE_FILE_COULDNT_READ_FILE"  },
    { CURLE_LDAP_CANNOT_BIND,           "CURLE_LDAP_CANNOT_BIND"        },
    { CURLE_LDAP_SEARCH_FAILED,         "CURLE_LDAP_SEARCH_FAILED"      },
    { CURLE_OBSOLETE40,                 "CURLE_OBSOLETE40"              },
    { CURLE_FUNCTION_NOT_FOUND,         "CURLE_FUNCTION_NOT_FOUND"      },
    { CURLE_ABORTED_BY_CALLBACK,        "CURLE_ABORTED_BY_CALLBACK"     },
    { CURLE_BAD_FUNCTION_ARGUMENT,      "CURLE_BAD_FUNCTION_ARGUMENT"   },
    { CURLE_OBSOLETE44,                 "CURLE_OBSOLETE44"              },
    { CURLE_INTERFACE_FAILED,           "CURLE_INTERFACE_FAILED"        },
    { CURLE_OBSOLETE46,                 "CURLE_OBSOLETE46"              },
    { CURLE_UNKNOWN_OPTION,             "CURLE_UNKNOWN_OPTION"          },
    { CURLE_TELNET_OPTION_SYNTAX,       "CURLE_TELNET_OPTION_SYNTAX"    },
    { CURLE_OBSOLETE50,                 "CURLE_OBSOLETE50"              },
    { CURLE_PEER_FAILED_VERIFICATION,   "CURLE_PEER_FAILED_VERIFICATION"},
    { CURLE_GOT_NOTHING,                "CURLE_GOT_NOTHING"             },
    { CURLE_SSL_ENGINE_NOTFOUND,        "CURLE_SSL_ENGINE_NOTFOUND"     },
    { CURLE_SSL_ENGINE_SETFAILED,       "CURLE_SSL_ENGINE_SETFAILED"    },
    { CURLE_SEND_ERROR,                 "CURLE_SEND_ERROR"              },
    { CURLE_RECV_ERROR,                 "CURLE_RECV_ERROR"              },
    { CURLE_OBSOLETE57,                 "CURLE_OBSOLETE57"              },
    { CURLE_SSL_CERTPROBLEM,            "CURLE_SSL_CERTPROBLEM"         },
    { CURLE_SSL_CIPHER,                 "CURLE_SSL_CIPHER"              },
    { CURLE_SSL_CACERT,                 "CURLE_SSL_CACERT"              },
    { CURLE_BAD_CONTENT_ENCODING,       "CURLE_BAD_CONTENT_ENCODING"    },
    { CURLE_LDAP_INVALID_URL,           "CURLE_LDAP_INVALID_URL"        },
    { CURLE_FILESIZE_EXCEEDED,          "CURLE_FILESIZE_EXCEEDED"       },
    { CURLE_USE_SSL_FAILED,             "CURLE_USE_SSL_FAILED"          },
    { CURLE_SEND_FAIL_REWIND,           "CURLE_SEND_FAIL_REWIND"        },
    { CURLE_SSL_ENGINE_INITFAILED,      "CURLE_SSL_ENGINE_INITFAILED"   },
    { CURLE_LOGIN_DENIED,               "CURLE_LOGIN_DENIED"            },
    { CURLE_TFTP_NOTFOUND,              "CURLE_TFTP_NOTFOUND"           },
    { CURLE_TFTP_PERM,                  "CURLE_TFTP_PERM"               },
    { CURLE_REMOTE_DISK_FULL,           "CURLE_REMOTE_DISK_FULL"        },
    { CURLE_TFTP_ILLEGAL,               "CURLE_TFTP_ILLEGAL"            },
    { CURLE_TFTP_UNKNOWNID,             "CURLE_TFTP_UNKNOWNID"          },
    { CURLE_REMOTE_FILE_EXISTS,         "CURLE_REMOTE_FILE_EXISTS"      },
    { CURLE_TFTP_NOSUCHUSER,            "CURLE_TFTP_NOSUCHUSER"         },
    { CURLE_CONV_FAILED,                "CURLE_CONV_FAILED"             },
    { CURLE_CONV_REQD,                  "CURLE_CONV_REQD"               },
    { CURLE_SSL_CACERT_BADFILE,         "CURLE_SSL_CACERT_BADFILE"      },
    { CURLE_REMOTE_FILE_NOT_FOUND,      "CURLE_REMOTE_FILE_NOT_FOUND"   },
    { CURLE_SSH,                        "CURLE_SSH"                     },
    { CURLE_SSL_SHUTDOWN_FAILED,        "CURLE_SSL_SHUTDOWN_FAILED"     },
    { CURLE_AGAIN,                      "CURLE_AGAIN"                   },
    { CURLE_SSL_CRL_BADFILE,            "CURLE_SSL_CRL_BADFILE"         },
    { CURLE_SSL_ISSUER_ERROR,           "CURLE_SSL_ISSUER_ERROR"        },
    { CURLE_FTP_PRET_FAILED,            "CURLE_FTP_PRET_FAILED"         },
    { CURLE_RTSP_CSEQ_ERROR,            "CURLE_RTSP_CSEQ_ERROR"         },
    { CURLE_RTSP_SESSION_ERROR,         "CURLE_RTSP_SESSION_ERROR"      },
    { CURLE_FTP_BAD_FILE_LIST,          "CURLE_FTP_BAD_FILE_LIST"       },
    { CURLE_CHUNK_FAILED,               "CURLE_CHUNK_FAILED"            },
    { CURLE_NO_CONNECTION_AVAILABLE,    "CURLE_NO_CONNECTION_AVAILABLE" },
    { CURLE_SSL_PINNEDPUBKEYNOTMATCH,   "CURLE_SSL_PINNEDPUBKEYNOTMATCH"},
    { CURLE_SSL_INVALIDCERTSTATUS,      "CURLE_SSL_INVALIDCERTSTATUS"   },
    { CURLE_HTTP2_STREAM,               "CURLE_HTTP2_STREAM"            }
};



CurlResponse::CurlResponse() : 
    _responseCode(0)
{

}


CurlResponse::CurlResponse(int responseCode, const std::string& headers) :
    _responseCode(responseCode),
    _headers(headers)
{
    updateHeadersMap();
}


CurlResponse::~CurlResponse()
{

}


void CurlResponse::setResponseCode(int responseCode)
{
    _responseCode = responseCode;
}


int CurlResponse::responseCode() const
{
    return _responseCode;
}


void CurlResponse::setHeaders(const std::string& headers)
{
    _headers = headers;
    updateHeadersMap();
}


const std::string& CurlResponse::headers() const
{
    return _headers;
}


const std::map<std::string, std::string>& CurlResponse::headersMap() const
{
    return _headersMap;
}


void CurlResponse::updateHeadersMap()
{
    const std::regex HEADER_REGEX("(\\S+):\\s(.*)");

    // split the lines
    std::vector<std::string> lines;
    std::string temp;

    for (char c : headers())
    {
        switch (c)
        {
        case '\r':
        case '\n':
            if (temp.size())
            {
                lines.push_back(temp);
            }
            temp.clear();
            break;

        default:
            temp += c;
        }
    }

    if (temp.size())
    {
        lines.push_back(temp);
    }

    _headersMap.clear();

    for (const std::string& line : lines)
    {
        std::smatch sm;

        if (std::regex_match(line, sm, HEADER_REGEX))
        {
            _headersMap.insert(std::make_pair(sm[1], sm[2]));
        }
    }
}

bool CurlResponse::success() const
{
    return responseCategory() == HttpResponseCategory::SUCCESS;
}


HttpResponseCategory CurlResponse::responseCategory() const
{
    int result = responseCode() / 100;
    switch (result)
    {
    case 1:
        return HttpResponseCategory::INFORMATIONAL;

    case 2:
        return HttpResponseCategory::SUCCESS;

    case 3:
        return HttpResponseCategory::REDIRECTION;

    case 4:
        return HttpResponseCategory::CLIENT_ERROR;

    case 5:
        return HttpResponseCategory::SERVER_ERROR;

    default:
        throw std::invalid_argument("Invalid response code");
    }
}



CurlResponseData::CurlResponseData()
{

}


CurlResponseData::CurlResponseData(int responseCode, const std::string& headers, const std::string& data) : 
    CurlResponse(responseCode, headers),
    _data(data)
{

}


void CurlResponseData::setData(const std::string& data)
{
    _data = data;
}


const std::string& CurlResponseData::data() const
{
    return _data;
}


CurlHeaderHelper::CurlHeaderHelper() : 
    headers(nullptr)
{

}


CurlHeaderHelper::~CurlHeaderHelper()
{
    if (headers)
    {
        curl_slist_free_all(headers);
        headers = nullptr;
    }
}


const std::string CurlWrapper::ACCEPT_HEADER                = "Accept";
const std::string CurlWrapper::CONTENT_TYPE_HEADER          = "Content-Type";
const std::string CurlWrapper::CONTENT_ENCODING_HEADER      = "Content-Encoding";
const std::string CurlWrapper::CONTENT_LENGTH_HEADER        = "Content-Length";
const std::string CurlWrapper::CONTENT_MD5_HEADER           = "Content-MD5";
const std::string CurlWrapper::CONTENT_LANGUAGE_HEADER      = "Content-Language";
const std::string CurlWrapper::DATE_HEADER                  = "Date";
const std::string CurlWrapper::IF_MODIFIED_SINCE_HEADER     = "If-Modified-Since";
const std::string CurlWrapper::IF_MATCH_HEADER              = "If-Match";
const std::string CurlWrapper::IF_NONE_MATCH_HEADER         = "If-None-Match";
const std::string CurlWrapper::IF_UNMODIFIED_SINCE_HEADER   = "If-Unmodified-Since";
const std::string CurlWrapper::RANGE_HEADER                 = "Range";


std::string CurlWrapper::certDir;
std::string CurlWrapper::certInfo;
bool CurlWrapper::_defaultVerbosity = false;
bool CurlWrapper::_defaultUseHttp2 = true;

void CurlWrapper::initCurl()
{
	CURLcode code = curl_global_init(CURL_GLOBAL_ALL);
	if (code != CURLE_OK)
	{
		std::stringstream ss;
		ss << "curl_global_init returned " << (int)code;
		throw std::runtime_error(ss.str());
	}
}


void CurlWrapper::cleanupCurl()
{
	curl_global_cleanup();
}


CurlWrapper::CurlWrapper() : 
    _followRedirect(false), 
    _verbose(false), 
    _maxUploadSpeed(0), 
    _maxDownloadSpeed(0),
    _useHttp2(CurlWrapper::defaultUseHttp2())
{
    
}


void CurlWrapper::setUrl(const std::string& url)
{
	_url = url;
}


const std::string& CurlWrapper::url() const
{
	return _url;
}


void CurlWrapper::setFollowRedirect(bool on)
{
	_followRedirect = on;
}


bool CurlWrapper::followRedirect() const
{
	return _followRedirect;
}


void CurlWrapper::setVerbose(bool on)
{
	_verbose = on;
}


bool CurlWrapper::verbose() const
{
	return _verbose;
}


void CurlWrapper::setUseHttp2(bool http2)
{
    _useHttp2 = http2;
}


bool CurlWrapper::useHttp2() const
{
    return _useHttp2;
}



void CurlWrapper::setCertDir(const std::string& certDir)
{
    CurlWrapper::certDir = certDir;
}


void CurlWrapper::setCertInfo(const std::string& certInfo)
{
    CurlWrapper::certInfo = certInfo;
}



void CurlWrapper::setDefaultVerbosity(bool verbose)
{
    _defaultVerbosity = verbose;
}


bool CurlWrapper::defaultVerbosity()
{
    return _defaultVerbosity;
}


void CurlWrapper::setDefaultUseHttp2(bool useHttp2)
{
    _defaultUseHttp2 = useHttp2;
}


bool CurlWrapper::defaultUseHttp2()
{
    return _defaultUseHttp2;
}



void CurlWrapper::addParam(const std::string& param, const std::string& value)
{
	_params.push_back(std::pair<std::string, std::string>(param, value));
}


void CurlWrapper::clearParams()
{
	_params.clear();
}


void CurlWrapper::addHeader(const std::string& header, const std::string& value)
{
	_headers.push_back(std::pair<std::string, std::string>(header, value));
}

void CurlWrapper::clearHeaders()
{
	_headers.clear();
}


void CurlWrapper::reset()
{
    clearParams();
    clearHeaders();
    _url.clear();
}



void CurlWrapper::populateCurlHeaders(curl_slist*& curlHeaders) const
{
	for (const auto& header : _headers)
	{
		std::string headerString = header.first + ": " + header.second;
		curlHeaders = curl_slist_append(curlHeaders, headerString.c_str());
	}
}



void CurlWrapper::setDefaultParams(CURL* handle) const
{
    CURLcode code;

    if (_followRedirect)
    {
        code = curl_easy_setopt(handle, CURLOPT_FOLLOWLOCATION, 1L);
        validateCode(handle, code);
    }
        
    if (verbose() || defaultVerbosity())
    {
        code = curl_easy_setopt(handle, CURLOPT_VERBOSE, 1L);
        validateCode(handle, code);
    }
        
    if (_maxUploadSpeed)
    {
        code = curl_easy_setopt(handle, CURLOPT_MAX_SEND_SPEED_LARGE, _maxUploadSpeed);
        validateCode(handle, code);
    }

    if (_maxDownloadSpeed)
    {
        code = curl_easy_setopt(handle, CURLOPT_MAX_RECV_SPEED_LARGE, _maxDownloadSpeed);
        validateCode(handle, code);
    }

    if (!certDir.empty())
    {
        code = curl_easy_setopt(handle, CURLOPT_CAPATH, certDir.c_str());
        validateCode(handle, code);
    }

    if (!certInfo.empty())
    {
        code = curl_easy_setopt(handle, CURLOPT_CAINFO, certInfo.c_str());
        validateCode(handle, code);
    }

    if (useHttp2())
    {
        code = curl_easy_setopt(handle, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_2_0);
        validateCode(handle, code);
    }
}



CurlResponseData CurlWrapper::get() const
{
    CURL* handle = curl_easy_init();
    validateHandle(handle);

	std::stringstream dataStream;
	std::stringstream headerStream;

    CurlHeaderHelper headerHelper;
	populateCurlHeaders(headerHelper.headers);
    CURLcode code = curl_easy_setopt(handle, CURLOPT_HTTPHEADER, headerHelper.headers);
    
	std::string url2 = completeUrl();

    setDefaultParams(handle);

	code = curl_easy_setopt(handle, CURLOPT_URL, url2.c_str());
    validateCode(handle, code);
	
    code = curl_easy_setopt(handle, CURLOPT_WRITEDATA, &dataStream);
    validateCode(handle, code);
	
    code = curl_easy_setopt(handle, CURLOPT_WRITEFUNCTION, &CurlWrapper::writeStringCallback);
    validateCode(handle, code);
	
    code = curl_easy_setopt(handle, CURLOPT_HEADER, 0L);
    validateCode(handle, code);
	
    code = curl_easy_setopt(handle, CURLOPT_HEADERDATA, &headerStream);
    validateCode(handle, code);
	
    code = curl_easy_setopt(handle, CURLOPT_HEADERFUNCTION, &CurlWrapper::writeStringCallback);
    validateCode(handle, code);
	
    code = curl_easy_setopt(handle, CURLOPT_HTTPGET, 1L);
    validateCode(handle, code);
	
    setDefaultParams(handle);

	code = curl_easy_perform(handle);
    validateCode(handle, code);

	long responseCodeLong = 0;
	code = curl_easy_getinfo(handle, CURLINFO_RESPONSE_CODE, &responseCodeLong);
    validateCode(handle, code);

	curl_easy_cleanup(handle);

    CurlResponseData crd;
    crd.setData(dataStream.str());
	crd.setHeaders(headerStream.str());
	crd.setResponseCode((int)responseCodeLong);

    return crd;
}


CurlResponse CurlWrapper::getFile(const std::string& filename) const
{
	CURL* handle = curl_easy_init();
    validateHandle(handle);

	std::ofstream dataStream;
	dataStream.open(filename, std::ios::binary | std::ios::out);

	std::stringstream headerStream;

    CurlHeaderHelper headerHelper;
	populateCurlHeaders(headerHelper.headers);
    CURLcode code = curl_easy_setopt(handle, CURLOPT_HTTPHEADER, headerHelper.headers);
    validateCode(handle, code);

	std::string url2 = completeUrl();

    code = curl_easy_setopt(handle, CURLOPT_URL, url2.c_str());
    validateCode(handle, code);

	code = curl_easy_setopt(handle, CURLOPT_WRITEDATA, &dataStream);
    validateCode(handle, code);

	code = curl_easy_setopt(handle, CURLOPT_WRITEFUNCTION, &CurlWrapper::writeFileCallback);
    validateCode(handle, code);

	code = curl_easy_setopt(handle, CURLOPT_HEADER, 0L);
    validateCode(handle, code);

	code = curl_easy_setopt(handle, CURLOPT_HEADERDATA, &headerStream);
    validateCode(handle, code);

    code = curl_easy_setopt(handle, CURLOPT_HEADERFUNCTION, &CurlWrapper::writeStringCallback);
    validateCode(handle, code);

    code = curl_easy_setopt(handle, CURLOPT_HTTPGET, 1L);
    validateCode(handle, code);

    setDefaultParams(handle);

	code = curl_easy_perform(handle);
	
    validateCode(handle, code);

	long responseCodeLong = 0;
	code = curl_easy_getinfo(handle, CURLINFO_RESPONSE_CODE, &responseCodeLong);
    validateCode(handle, code);

    curl_easy_cleanup(handle);

	dataStream.close();
	
    std::string headers = headerStream.str();

	int responseCode = (int)responseCodeLong;

    return CurlResponse(responseCode, headers);
}



CurlResponseData CurlWrapper::put(const std::string& data) const
{
	CURL* handle = curl_easy_init();
    validateHandle(handle);

    std::stringstream ss(data, std::ios_base::in);
    std::stringstream headerStream;
    std::stringstream responseStream;

    CurlHeaderHelper headerHelper;
    populateCurlHeaders(headerHelper.headers);

    std::string url2 = completeUrl();

    CURLcode code = curl_easy_setopt(handle, CURLOPT_UPLOAD, 1L);
    validateCode(handle, code);

    code = curl_easy_setopt(handle, CURLOPT_PUT, 1L);
    validateCode(handle, code);

    code = curl_easy_setopt(handle, CURLOPT_HTTPHEADER, headerHelper.headers);
    validateCode(handle, code);

    code = curl_easy_setopt(handle, CURLOPT_HEADER, 0L);
    validateCode(handle, code);

    code = curl_easy_setopt(handle, CURLOPT_HEADERDATA, &headerStream);
    validateCode(handle, code);

    code = curl_easy_setopt(handle, CURLOPT_HEADERFUNCTION, &CurlWrapper::writeStringCallback);
    validateCode(handle, code);

    code = curl_easy_setopt(handle, CURLOPT_URL, url2.c_str());
    validateCode(handle, code);
	
    code = curl_easy_setopt(handle, CURLOPT_READFUNCTION, &CurlWrapper::readStringCallback);
    validateCode(handle, code);

    code = curl_easy_setopt(handle, CURLOPT_READDATA, &ss);
    validateCode(handle, code);

    code = curl_easy_setopt(handle, CURLOPT_WRITEFUNCTION, &CurlWrapper::writeStringCallback);
    validateCode(handle, code);

    code = curl_easy_setopt(handle, CURLOPT_WRITEDATA, &responseStream);
    validateCode(handle, code);

    setDefaultParams(handle);

	code = curl_easy_perform(handle);
    validateCode(handle, code);

    long responseCodeLong;
    code = curl_easy_getinfo(handle, CURLINFO_RESPONSE_CODE, &responseCodeLong);
    validateCode(handle, code);

    curl_easy_cleanup(handle);

    CurlResponseData response;
    response.setData(responseStream.str());
    response.setResponseCode((int)responseCodeLong);
    response.setHeaders(headerStream.str());
	
    return response;
}


CurlResponseData CurlWrapper::put(const void* buffer, size_t size) const
{
    CURL* handle = curl_easy_init();
    validateHandle(handle);

    BufferCallback bc;
    bc.buffer_ = (char*) buffer;
    bc.buflen_ = (int) size;
    bc.bufpos_ = 0;

    std::stringstream headerStream;
    std::stringstream responseStream;

    CurlHeaderHelper headerHelper;
    populateCurlHeaders(headerHelper.headers);

    std::string url = completeUrl();

    CURLcode code;
    code = curl_easy_setopt(handle, CURLOPT_UPLOAD, 1L);
    validateCode(handle, code);

    //code = curl_easy_setopt(handle, CURLOPT_PUT, 1L);
    //validateCode(handle, code);

    code = curl_easy_setopt(handle, CURLOPT_INFILESIZE_LARGE, (curl_off_t)size);
    validateCode(handle, code);

    code = curl_easy_setopt(handle, CURLOPT_HTTPHEADER, headerHelper.headers);
    validateCode(handle, code);

    code = curl_easy_setopt(handle, CURLOPT_HEADER, 0L);
    validateCode(handle, code);

    code = curl_easy_setopt(handle, CURLOPT_HEADERDATA, &headerStream);
    validateCode(handle, code);

    code = curl_easy_setopt(handle, CURLOPT_HEADERFUNCTION, &CurlWrapper::writeStringCallback);
    validateCode(handle, code);

    code = curl_easy_setopt(handle, CURLOPT_URL, url.c_str());
    validateCode(handle, code);

    code = curl_easy_setopt(handle, CURLOPT_READFUNCTION, &CurlWrapper::readBuffCallback);
    validateCode(handle, code);

    code = curl_easy_setopt(handle, CURLOPT_READDATA, &bc);
    validateCode(handle, code);

    code = curl_easy_setopt(handle, CURLOPT_WRITEFUNCTION, &CurlWrapper::writeStringCallback);
    validateCode(handle, code);

    code = curl_easy_setopt(handle, CURLOPT_WRITEDATA, &responseStream);
    validateCode(handle, code);

    setDefaultParams(handle);

    code = curl_easy_perform(handle);
    validateCode(handle, code);

    long responseCodeLong;
    code = curl_easy_getinfo(handle, CURLINFO_RESPONSE_CODE, &responseCodeLong);
    validateCode(handle, code);

    curl_easy_cleanup(handle);

    CurlResponseData crd;
    crd.setResponseCode((int)responseCodeLong);
    crd.setHeaders(headerStream.str());
    crd.setData(responseStream.str());

    return crd;
}


CurlResponseData CurlWrapper::putFile(const std::string& path)
{
	CURL* handle = curl_easy_init();

    std::ifstream dataStream;
	dataStream.open(path, std::ios_base::in | std::ios_base::binary);
	if (!dataStream.is_open())
    {
        std::string msg = "Unable to open file: " + path;
        throw std::invalid_argument(msg);
	}

	dataStream.seekg(0, dataStream.end);
    std::streampos fileLength = dataStream.tellg();
	dataStream.seekg(0, dataStream.beg);

    std::stringstream headerStream;
    std::stringstream responseStream;

    CurlHeaderHelper headerHelper;
	
	populateCurlHeaders(headerHelper.headers);
    CURLcode code = curl_easy_setopt(handle, CURLOPT_HTTPHEADER, headerHelper.headers);

    std::string url2 = completeUrl();

	code = curl_easy_setopt(handle, CURLOPT_URL, url2.c_str());
    validateCode(handle, code);

    code = curl_easy_setopt(handle, CURLOPT_READDATA, &dataStream);
    validateCode(handle, code);

    code = curl_easy_setopt(handle, CURLOPT_READFUNCTION, &CurlWrapper::readFileCallback);
    validateCode(handle, code);

    code = curl_easy_setopt(handle, CURLOPT_HEADER, 1L);
    validateCode(handle, code);

    code = curl_easy_setopt(handle, CURLOPT_HEADERDATA, &headerStream);
    validateCode(handle, code);

    code = curl_easy_setopt(handle, CURLOPT_HEADERFUNCTION, &CurlWrapper::writeStringCallback);
    validateCode(handle, code);

    code = curl_easy_setopt(handle, CURLOPT_WRITEFUNCTION, &CurlWrapper::writeStringCallback);
    validateCode(handle, code);

    code = curl_easy_setopt(handle, CURLOPT_WRITEDATA, &responseStream);
    validateCode(handle, code);


    code = curl_easy_setopt(handle, CURLOPT_UPLOAD, 1L);
    assert(code == CURLE_OK);
    code = curl_easy_setopt(handle, CURLOPT_INFILESIZE, (long) fileLength);
    assert(code == CURLE_OK);

    setDefaultParams(handle);

	code = curl_easy_perform(handle);
    validateCode(handle, code);

	dataStream.close();

	long responseCodeLong;
	curl_easy_getinfo(handle, CURLINFO_RESPONSE_CODE, &responseCodeLong);

	curl_easy_cleanup(handle);

    CurlResponseData crd;
    crd.setResponseCode((int)responseCodeLong);
    crd.setHeaders(headerStream.str());
    crd.setData(responseStream.str());

    return crd;
}


CurlResponseData CurlWrapper::delete_() const
{
    CURL* handle = curl_easy_init();
    validateHandle(handle);

    std::stringstream headerStream;
    std::stringstream responseStream;

    CurlHeaderHelper headerHelper;
    populateCurlHeaders(headerHelper.headers);

    std::string url = completeUrl();

    CURLcode code;
    
    code = curl_easy_setopt(handle, CURLOPT_UPLOAD, 1L);
    validateCode(handle, code);

    code = curl_easy_setopt(handle, CURLOPT_CUSTOMREQUEST, "DELETE");
    validateCode(handle, code);
  
    code = curl_easy_setopt(handle, CURLOPT_HTTPHEADER, headerHelper.headers);
    validateCode(handle, code);

    code = curl_easy_setopt(handle, CURLOPT_HEADER, 0L);
    validateCode(handle, code);

    code = curl_easy_setopt(handle, CURLOPT_HEADERDATA, &headerStream);
    validateCode(handle, code);

    code = curl_easy_setopt(handle, CURLOPT_HEADERFUNCTION, &CurlWrapper::writeStringCallback);
    validateCode(handle, code);

    code = curl_easy_setopt(handle, CURLOPT_URL, url.c_str());
    validateCode(handle, code);

    code = curl_easy_setopt(handle, CURLOPT_WRITEFUNCTION, &CurlWrapper::writeStringCallback);
    validateCode(handle, code);

    code = curl_easy_setopt(handle, CURLOPT_WRITEDATA, &responseStream);
    validateCode(handle, code);

    setDefaultParams(handle);

    code = curl_easy_perform(handle);
    validateCode(handle, code);

    long responseCodeLong;
    code = curl_easy_getinfo(handle, CURLINFO_RESPONSE_CODE, &responseCodeLong);
    validateCode(handle, code);

    curl_easy_cleanup(handle);

    CurlResponseData crd;
    crd.setResponseCode((int)responseCodeLong);
    crd.setHeaders(headerStream.str());
    crd.setData(responseStream.str());

    return crd;
}



#ifdef REFACTOR_LATER

int CurlWrapper::post(const std::string& postData, std::string& receivedData, std::string& headers, int& responseCode) const
{
	CURL* handle = curl_easy_init();

    std::stringstream dataStream(postData, std::ios_base::in);
    std::stringstream headerStream;
    std::stringstream responseStream;

	curl_slist* curlHeaders = NULL;
	populateCurlHeaders(curlHeaders);
    curl_easy_setopt(handle, CURLOPT_HTTPHEADER, curlHeaders);

    std::string url2 = completeUrl();

    CURLcode code;
	code = curl_easy_setopt(handle, CURLOPT_URL, url2.c_str());
    assert(code == CURLE_OK);
    code = curl_easy_setopt(handle, CURLOPT_POSTFIELDSIZE, postData.size());
    assert(code == CURLE_OK);
    code = curl_easy_setopt(handle, CURLOPT_READDATA, &dataStream);
    assert(code == CURLE_OK);
    code = curl_easy_setopt(handle, CURLOPT_READFUNCTION, &CurlWrapper::readStringCallback);
    assert(code == CURLE_OK);
    code = curl_easy_setopt(handle, CURLOPT_HEADER, 0L);
    assert(code == CURLE_OK);
    code = curl_easy_setopt(handle, CURLOPT_HEADERDATA, &headerStream);
    assert(code == CURLE_OK);
    code = curl_easy_setopt(handle, CURLOPT_HEADERFUNCTION, &CurlWrapper::writeStringCallback);
    assert(code == CURLE_OK);
    code = curl_easy_setopt(handle, CURLOPT_WRITEDATA, &responseStream);
    assert(code == CURLE_OK);
    code = curl_easy_setopt(handle, CURLOPT_WRITEFUNCTION, &CurlWrapper::writeStringCallback);
    assert(code == CURLE_OK);
    code = curl_easy_setopt(handle, CURLOPT_POST, 1L);
    assert(code == CURLE_OK);

    setDefaultParams(handle);

	code = curl_easy_perform(handle);
	curl_slist_free_all(curlHeaders);

	if (code != CURLE_OK)
	{
		curl_easy_cleanup(handle);
		return code;
	}

	long responseCodeLong;
	curl_easy_getinfo(handle, CURLINFO_RESPONSE_CODE, &responseCodeLong);

	curl_easy_cleanup(handle);

	headers = headerStream.str();
    receivedData = responseStream.str();
	responseCode = (int)responseCodeLong;

    return CURLE_OK;
}


int CurlWrapper::multipartPost(const std::map<std::string, std::string>& data, const std::map<std::string, std::string>& files,
    std::string& receivedData, std::string& headers, int& responseCode) const
{
    CURL* handle = curl_easy_init();

    std::stringstream headerStream;
    std::stringstream responseStream;

    struct curl_httppost* post = NULL;
    struct curl_httppost* last = NULL;

    for (const auto& d : data)
    {
        CURLFORMcode code = curl_formadd(&post, &last, CURLFORM_PTRNAME, d.first.c_str(),
            CURLFORM_PTRCONTENTS, d.second.c_str(), CURLFORM_END);
        assert(code == CURL_FORMADD_OK);
    }

    for (const auto& file : files)
    {
        CURLFORMcode code = curl_formadd(&post, &last, CURLFORM_PTRNAME, file.first.c_str(), 
            CURLFORM_FILE, file.second.c_str(), CURLFORM_END);
        assert(code == CURL_FORMADD_OK);
    }

    curl_slist* curlHeaders = NULL;
    populateCurlHeaders(curlHeaders);
    curl_easy_setopt(handle, CURLOPT_HTTPHEADER, curlHeaders);

    std::string url2 = completeUrl();

    CURLcode code;
    code = curl_easy_setopt(handle, CURLOPT_URL, url2.c_str());
    assert(code == CURLE_OK);
    code = curl_easy_setopt(handle, CURLOPT_HEADER, 0L);
    assert(code == CURLE_OK);
    code = curl_easy_setopt(handle, CURLOPT_HEADERDATA, &headerStream);
    assert(code == CURLE_OK);
    code = curl_easy_setopt(handle, CURLOPT_HEADERFUNCTION, &CurlWrapper::writeStringCallback);
    assert(code == CURLE_OK);
    code = curl_easy_setopt(handle, CURLOPT_WRITEDATA, &responseStream);
    assert(code == CURLE_OK);
    code = curl_easy_setopt(handle, CURLOPT_WRITEFUNCTION, &CurlWrapper::writeStringCallback);
    assert(code == CURLE_OK);

    code = curl_easy_setopt(handle, CURLOPT_HTTPPOST, post);
    assert(code == CURLE_OK);

    setDefaultParams(handle);

    code = curl_easy_perform(handle);
    curl_slist_free_all(curlHeaders);

    if (code != CURLE_OK)
    {
        curl_easy_cleanup(handle);
        return code;
    }

    long responseCodeLong;
    curl_easy_getinfo(handle, CURLINFO_RESPONSE_CODE, &responseCodeLong);

    curl_easy_cleanup(handle);

    headers = headerStream.str();
    receivedData = responseStream.str();
    responseCode = (int)responseCodeLong;

    return CURLE_OK;
}

#endif


size_t CurlWrapper::readStringCallback(char* contents, size_t size, size_t nmemb, void* userp)
{
    std::stringstream& ss = *((std::stringstream*)userp);

	return (size_t)ss.readsome(contents, size * nmemb);
}


size_t CurlWrapper::readBuffCallback(char* contents, size_t size, size_t nmemb, void* userp)
{
    BufferCallback* bc = (BufferCallback*) userp;

    size_t reqSize = size * nmemb;
    size_t bytesToCopy = std::min(reqSize, (size_t) bc->buflen_ - (size_t) bc->bufpos_);
    memcpy(contents, &bc->buffer_[bc->bufpos_], bytesToCopy);
    bc->bufpos_ += (int) bytesToCopy;

    return bytesToCopy;
}


size_t CurlWrapper::readFileCallback(char* contents, size_t size, size_t nmemb, void* userp)
{
    std::ifstream& fs = *((std::ifstream*)userp);

	fs.read(contents, size*nmemb);
    std::streamsize bytesRead = fs.gcount();

	return (size_t) bytesRead;
}


size_t CurlWrapper::writeStringCallback(void * contents, size_t size, size_t nmemb, void * userp)
{
    std::stringstream& ss = *((std::stringstream*) userp);

	ss.write( (const char*) contents, size * nmemb);
	
	return size * nmemb;
}


size_t CurlWrapper::writeFileCallback(void * contents, size_t size, size_t nmemb, void * userp)
{
    std::ofstream& fs = *((std::ofstream*)userp);

	fs.write((const char*)contents, size * nmemb);

	return size * nmemb;
}



const std::string CurlWrapper::PARAMS_DELIM = "&";
const std::string CurlWrapper::KEY_VALUE_DELIM = "=";
const std::string CurlWrapper::PARAMS_SEPARATOR = "?";


std::string CurlWrapper::encodedParams() const
{
    std::vector<std::string> encodedParams;

	for (const auto& p : _params)
	{
        std::string encodedFirst = urlEncode(p.first);
        std::string encodedSecond = urlEncode(p.second);
		encodedParams.push_back(encodedFirst + KEY_VALUE_DELIM + encodedSecond);
	}

	return joinStrings(encodedParams, PARAMS_DELIM);
}


std::string CurlWrapper::completeUrl() const
{
	return _params.empty() ? _url : (_url + PARAMS_SEPARATOR + encodedParams());
}




std::string CurlWrapper::joinStrings(const std::vector<std::string>& strings, const std::string& delim)
{
    std::string j = strings.front();

	auto i = strings.begin();
	++i;
	
	while (i != strings.end())
	{
		j += delim;
		j += *i;
		i++;
	}

	return j;
}


std::string CurlWrapper::urlEncode(const std::string& s)
{
    const std::string VALID_URL_CHARS("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-._~:");

    std::stringstream enc;

    for (const char& c : s)
    {
        if (find(std::begin(VALID_URL_CHARS), std::end(VALID_URL_CHARS), c) != std::end(VALID_URL_CHARS))
            enc << c;
        else
        {
            const int BUFLEN = 10;
            char hex[BUFLEN];
            snprintf(hex, BUFLEN, "%%%02X", (unsigned char)c);
            enc.write(hex, strnlen(hex, BUFLEN));
        }
    }

    std::string result = enc.str();

    return result;
}


std::string CurlWrapper::urlDecode(const std::string& s)
{
    std::string ret;

    for (int i = 0; i < s.length(); i++)
    {
        if (s.at(i) != '%')
        {
            if (s.at(i) == '+')
            {
                ret += ' ';
            }
            else
            {
                ret += s.at(i);
            }
        }
        else
        {
            std::string substr = s.substr(i+1, 2);
            int intValue = std::stoi(substr, nullptr, 16);
            ret += static_cast<char>(intValue);

            i += 2;
        }
    }

    return ret;
}




void CurlWrapper::validateCode(CURL* handle, CURLcode code)
{
    if (code != CURLE_OK)
    {
        curl_easy_cleanup(handle);
        throw CurlCodeException(code);
    }
}



void CurlWrapper::validateHandle(CURL* handle)
{
    if (handle == nullptr)
    {
        throw CurlException("CURL handle is null");
    }
}

