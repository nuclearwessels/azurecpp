

#ifndef STRING_UTILS_H
#define STRING_UTILS_H

#include <string>
#include <vector>


namespace azurecpp
{

std::string join(const std::string& separator, const std::vector<std::string>& strings);

std::vector<std::string> split(const std::string& split, const std::string& splitChars, bool addEmpty = false);


std::string hexString(const void* b, int len);
std::string hexString(const std::string& s);
std::string hexString(const std::vector<unsigned char>& v);

std::string toLower(const std::string& s);
std::string toUpper(const std::string& s);

bool compare(const std::string& a, const std::string& b, bool caseSensitive = true);

bool beginsWith(const std::string& s, const std::string& beginning, bool caseSensitive = true);
bool endsWith(const std::string& s, const std::string& ending, bool caseSensitive = true);

std::string left(const std::string& s, int numChars);
std::string right(const std::string& s, int numChars);


}


#endif

