

#include "string_utils.h"


std::string azurecpp::join(const std::string& separator, const std::vector<std::string>& strings)
{
    if (strings.size() == 0)
        return std::string();

    std::string combined = strings[0];

    for (int i=1; i<strings.size(); i++)
    {
        combined += separator;
        combined += strings[i];
    }

    return combined;
}


std::vector<std::string> azurecpp::split(const std::string& split, const std::string& splitChars, bool addEmpty)
{
    std::string s;
    std::vector<std::string> splitStrings;

    for (char c : split)
    {
        if (splitChars.find(c) == std::string::npos)
            s += c;
        else
        {
            if (s.empty())
            {
                if (addEmpty)
                    splitStrings.push_back(s);
            }
            else
            {
                splitStrings.push_back(s);
                s.clear();
            }
        }
    }

    if (s.empty())
    {
        if (addEmpty)
            splitStrings.push_back(s);
    }
    else
    {
        splitStrings.push_back(s);
        s.clear();
    }

    return splitStrings;
}



std::string azurecpp::hexString(const void* b, int len)
{
    std::string s;

    const unsigned char* p = (const unsigned char*) b;

    char temp[10];

    for (int i = 0; i < len; i++)
    {
        snprintf(temp, 10, "%02X", p[i]);
        s += temp;
    }

    return s;
}


std::string azurecpp::hexString(const std::string& bytes)
{
    std::string s;

    char temp[10];

    for (char c : bytes)
    {
        snprintf(temp, 10, "%02X", (unsigned char) c);
        s += temp;
    }

    return s;
}



std::string azurecpp::hexString(const std::vector<unsigned char>& v)
{
    std::string s;

    char temp[10];

    for (unsigned char c : v)
    {
        snprintf(temp, 10, "%02X", c);
        s += temp;
    }

    return s;
}



std::string azurecpp::toLower(const std::string& s)
{
    std::string r;
    for (char c : s)
    {
        r += tolower(c);
    }

    return r;
}


std::string azurecpp::toUpper(const std::string& s)
{
    std::string r;
    for (char c : s)
    {
        r += toupper(c);
    }

    return r;
}



bool azurecpp::compare(const std::string& a, const std::string& b, bool caseSensitive)
{
    if (a.length() != b.length())
        return false;

    if (caseSensitive)
    {
        for (auto aa = a.cbegin(), bb = b.cbegin(); aa != a.cend(); ++aa, ++bb)
        {
            if (*aa != *bb)
                return false;
        }
    }
    else
    {
        for (auto aa = a.cbegin(), bb = b.cbegin(); aa != a.cend(); ++aa, ++bb)
        {
            int aaLower = ::tolower(*aa);
            int bbLower = ::tolower(*bb);

            if (aaLower != bbLower)
                return false;
        }
    }

    return true;
}


std::string azurecpp::left(const std::string& s, int numChars)
{
    if (s.length() <= numChars)
        return s;
    else
        return s.substr(0, numChars);
}


std::string azurecpp::right(const std::string& s, int numChars)
{
    if (s.length() <= numChars)
        return s;
    else
    {
        size_t pos = s.length() - numChars;
        return s.substr(pos, numChars);
    }
}


bool azurecpp::beginsWith(const std::string& s, const std::string& beginning, bool caseSensitive)
{
    std::string l = left(s, (int) beginning.length());
    return compare(beginning, l, caseSensitive);
}



bool azurecpp::endsWith(const std::string& s, const std::string& ending, bool caseSensitive)
{
    std::string r = right(s, (int) ending.length());
    return azurecpp::compare(r, ending, caseSensitive);
}


