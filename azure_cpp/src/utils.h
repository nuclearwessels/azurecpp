#pragma once


#include <string>


namespace azurecpp
{
    std::string encodeDateTime(const time_t& t);

    std::string currentDateTime();
    
}

