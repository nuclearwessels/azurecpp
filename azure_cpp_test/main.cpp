

#define CATCH_CONFIG_RUNNER
#include "catch.hpp"

#include "curl_wrapper.h"
#include "azure_blob.h"

#include <string>


int main(int argc, char* argv[])
{
#ifdef _WINDOWS
    TCHAR buffer[MAX_PATH];
    memset(buffer, 0, MAX_PATH);

    GetModuleFileName(NULL, buffer, MAX_PATH - 1);

    std::string s(buffer);
    size_t lastSlashPos = s.find_last_of('\\');

    std::string certFile = s.substr(0, lastSlashPos) + "\\curl-ca-bundle.crt";

    CurlWrapper::setCertInfo(certFile);
    

#else 
#error Not set up for non-Windows build.
#endif

    azurecpp::BlobService::setDefaultConnErrRetries(1);

    CurlWrapper::setDefaultVerbosity(true);
    CurlWrapper::setDefaultUseHttp2(false);

    int result = Catch::Session().run(argc, argv);

    return result;
}

