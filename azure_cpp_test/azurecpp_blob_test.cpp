

#include "catch.hpp"
#include "azure_blob.h"
#include "base64.h"
#include <random>

static const std::string TEST_SAS_TOKEN = "?sv=2021-12-02&ss=bfqt&srt=sco&sp=rwdlacupiytfx&se=2024-03-05T07:46:56Z&st=2023-03-04T23:46:56Z&spr=https&sig=Ni9unp%2BiE7duabrI8svMtmhVeetwDrDI5vQBk3RqDrk%3D";
static const std::string TEST_KEY = "TU3UAP7ZowlB7WLQPgbxjKMlq7vQ1FzbE7eYze8ByGKBOWFGT5rRXq3krAfkR9cH2K0rCrptEhD0P7/5RQe0cg==";

static const std::string ACCOUNT = "qazureblob";
static const std::string CONTAINER = "blobs";


using namespace azurecpp;

static std::random_device rd;   // non-deterministic generator


static std::string randomName()
{
    static const std::string FIRST_CHAR("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789");
    static const std::string CHARS("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789 ");
    constexpr int LENGTH = 20;

    static std::mt19937 gen(rd());
    std::uniform_int_distribution<> dist(0, (int) (CHARS.size() - 1));

    std::string filename;

    filename += FIRST_CHAR.at(dist(gen));

    for (int i = 0; i < LENGTH; ++i) 
    {
        filename += CHARS.at(dist(gen));
    }

    filename += ".txt";

    return filename;
}



static void listContainersTest(BlobService& bs)
{
    ListContainerResponse lcr = bs.listContainers();
    REQUIRE(lcr.containers().size());
}


TEST_CASE("blob_list_containers", "[container]")
{
    {
        BlobService bs(ACCOUNT);
        bs.setSASToken(TEST_SAS_TOKEN);
        listContainersTest(bs);
    }

    {
        BlobService bs(ACCOUNT);
        bs.setAccountKey(TEST_KEY);
        listContainersTest(bs);
    }
}



TEST_CASE("blob_list_blobs", "[blob]")
{
    for (int i = 0; i < 2; i++)
    {
        BlobService bs(ACCOUNT);

        switch (i)
        {
        case 0:
            bs.setSASToken(TEST_SAS_TOKEN);
            break;

        case 1:
            bs.setAccountKey(TEST_KEY);
            break;
        }

        ListBlobOptions options;
        ListBlobResponse lbr = bs.listBlobs(CONTAINER, options);
        REQUIRE(lbr.blobs().size());
    }
}


TEST_CASE("blob_list_blobs_metadata", "[blob]")
{
    for (int i = 0; i < 2; i++)
    {
        BlobService bs(ACCOUNT);

        switch (i)
        {
        case 0:
            bs.setSASToken(TEST_SAS_TOKEN);
            break;

        case 1:
            bs.setAccountKey(TEST_KEY);
            break;
        }

        PutBlobOptions putOptions;
        putOptions.metadata = { std::make_pair("hello", "world") };

        std::string data = "Hi there!";

        bs.putBlob(CONTAINER, "metadata test.txt", putOptions, data.c_str(), data.length());

        std::map<std::string, std::string> tagMap =
        {
            std::make_pair("tag1", "test1"),
            std::make_pair("tag2", "test2")
        };

        bs.setBlobTags(CONTAINER, "metadata test.txt", tagMap);

        ListBlobOptions options;
        options.includeOptions.metadata = true;
        options.includeOptions.tags = true;
        options.prefix = "metadata test.txt";

        ListBlobResponse lbr = bs.listBlobs(CONTAINER, options);
        REQUIRE(lbr.blobs().size());

        const BlobInfo& blobInfo = lbr.blobs().front();

        REQUIRE(blobInfo.metadata().size());
        REQUIRE(blobInfo.metadata().at("hello") == "world");


        auto getTagMap = bs.getBlobTags(CONTAINER, "metadata test.txt");

        for (const auto& tag : tagMap)
        {
            REQUIRE(getTagMap[tag.first] == tag.second);
        }

        bs.deleteBlob(CONTAINER, "metadata test.txt", DeleteBlobOptions());
    }
}



TEST_CASE("blob_put_blob", "[blob]")
{
    const char* data = "Hello there";

    const std::vector<std::string> FILES =
    {
        "test_put.txt",
        "test folder/test put.txt"
    };

    for (const std::string& file : FILES)
    {
        for (int i = 0; i < 2; i++)
        {
            BlobService bs("qazureblob");

            switch (i)
            {
            case 0:
                bs.setSASToken(TEST_SAS_TOKEN);
                break;

            case 1:
                bs.setAccountKey(TEST_KEY);
                break;
            }

            PutBlobOptions options;
            bs.putBlob("blobs", file, options, data, (int) strlen(data));
        }
    }

}


static void putBlockTest(BlobService& bs, const std::string& filename)
{
    std::vector<PutBlockBlob> blockIds;

    for (int i = 0; i < 10; i++)
    {
        std::string data = "Hello there " + std::to_string(i);

        std::string blockId((char*)&i, sizeof(int));
        std::string blockIdB64 = base64_encode(blockId);

        PutBlockOptions options;
        bs.putBlock(CONTAINER, filename, blockIdB64, options, data.c_str(), (int) data.length());

        PutBlockBlob pbb;
        pbb.blockId = blockIdB64;
        pbb.blockType = BlockListType::UNCOMMITTED;
        blockIds.push_back(pbb);
    }

    PutBlockListOptions options;
    bs.putBlockList(CONTAINER, filename, options, blockIds);
}



TEST_CASE("blob_put_block", "[blob]")
{
    std::string file1 = randomName();
    std::string file2 = randomName();

    {
        BlobService bs(ACCOUNT);
        bs.setSASToken(TEST_SAS_TOKEN);
        putBlockTest(bs, file1);
    }
    

    {
        BlobService bs(ACCOUNT);
        bs.setAccountKey(TEST_KEY);
        putBlockTest(bs, file2);
    }
    
}


static void getBlobTest(BlobService& bs)
{
    const std::string DATA = "Hello world!";

    PutBlobOptions putOptions;
    putOptions.metadata.insert(std::make_pair("Field1", "Test"));
    putOptions.metadata.insert(std::make_pair("Field2", "Test2"));
    bs.putBlob(CONTAINER, "hello_world_get.txt", putOptions, DATA.c_str(), (int) DATA.size());

    GetBlobOptions getOptions;
    auto response = bs.getBlob(CONTAINER, "hello_world_get.txt", getOptions);

    REQUIRE(response.data() == DATA);
}



TEST_CASE("blob_get", "[blob]")
{
    BlobService bs(ACCOUNT);
    bs.setSASToken(TEST_SAS_TOKEN);
    getBlobTest(bs);

    bs.clear();

    bs.setAccount(ACCOUNT);
    bs.setAccountKey(TEST_KEY);
    getBlobTest(bs);
    

}


static void blobExists(BlobService& bs)
{
    const std::string TEST_DATA = "test data";

    std::string blobName = randomName();

    PutBlobOptions options;
    bs.putBlob(CONTAINER, blobName, options, TEST_DATA.c_str(), (int) TEST_DATA.size());

    bool exists = bs.blobExists(CONTAINER, blobName);

    REQUIRE(exists);
}


TEST_CASE("blob_exists", "[blob]")
{
    BlobService bs(ACCOUNT);
    bs.setSASToken(TEST_SAS_TOKEN);

    blobExists(bs);

    bs.clear();

    bs.setAccount(ACCOUNT);
    bs.setAccountKey(TEST_KEY);

    blobExists(bs);
}



static void deleteBlob(BlobService& bs)
{
    const std::string DATA = "data";

    std::string blobName = randomName();

    PutBlobOptions putBlobOptions;
    bs.putBlob(CONTAINER, blobName, putBlobOptions, DATA.c_str(), (int)DATA.length());

    bool exists = bs.blobExists(CONTAINER, blobName);
    REQUIRE(exists);

    DeleteBlobOptions deleteOptions;
    bs.deleteBlob(CONTAINER, blobName, deleteOptions);

    exists = bs.blobExists(CONTAINER, blobName);
    REQUIRE(!exists);

    std::string blobNameFail = randomName();
    REQUIRE(!bs.blobExists(CONTAINER, blobName));

    REQUIRE_THROWS(bs.deleteBlob(CONTAINER, blobName, deleteOptions));
}


TEST_CASE("blob_delete", "[blob]")
{
    BlobService bs(ACCOUNT);
    bs.setSASToken(TEST_SAS_TOKEN);
    
    deleteBlob(bs);

    bs.clear();

    bs.setAccount(ACCOUNT);
    bs.setAccountKey(TEST_KEY);
    deleteBlob(bs);
}


