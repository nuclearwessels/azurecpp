
#include "catch.hpp"
#include "curl_wrapper.h"


TEST_CASE("https_get", "[.][https_get]")
{
    CurlWrapper wrapper;

    wrapper.setUseHttp2(false);

    wrapper.setUrl("https://reqbin.com/echo/get/json");

    CurlResponseData crd = wrapper.get();

    REQUIRE(crd.responseCode() == 200);
}

